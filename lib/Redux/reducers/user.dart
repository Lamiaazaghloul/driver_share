

import 'package:driver_share/Redux/actions/userActions.dart';
import 'package:driver_share/Redux/states/user_state.dart';
import 'package:redux/redux.dart';


final userReducer = combineReducers<UserState>([
 TypedReducer <UserState,SetUsersAction>(setItems),
 TypedReducer <UserState,PushUsersAction>(psuhItems),
 TypedReducer <UserState,NewUsersAction>(newItems),

]);

UserState setItems(UserState state,SetUsersAction action)
{
  return UserState(
    items:[]..addAll(action.items),
  total: action.total);
}


UserState psuhItems(UserState state,PushUsersAction action)
{
  return UserState(
    items:[]..addAll(state.items)..addAll(action.items),
  total: action.total);
}


UserState newItems(UserState state,NewUsersAction action)
{
  return UserState(
    items:[action.user]..addAll(state.items),
  total: state.total !=null ?state.total +1 :null);
}
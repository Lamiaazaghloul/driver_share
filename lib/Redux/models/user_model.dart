class Users{
  final int id;
  final String fullName;
  final String email;
  final String phone;
  final String password;

  Users({this.id, this.fullName, this.email, this.phone, this.password});

  factory Users.fromJson(Map<String, dynamic> json) {
    return new Users(
      id: json['id'],
      fullName: json['fullName'],
      email: json['email'],
      phone: json['phone'],
      password: json['password'],
    
    );
  }
}
class OffersState{
  final List<OffersState> items;
  final int total;

  OffersState({this.items,this.total});

  factory OffersState.initial(){
    return OffersState(
      items: List.unmodifiable(<OffersState>[]),
    );
  }
  OffersState copywith(
  {
    List<OffersState> items,
    int total,
  }){
    return OffersState(items:items ??this.items ,total: total ??this.total);
  }

}
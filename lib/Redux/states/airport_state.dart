class AirportState{
  final List<AirportState> items;
  final int total;

  AirportState({this.items,this.total});

  factory AirportState.initial(){
    return AirportState(
      items: List.unmodifiable(<AirportState>[]),
    );
  }
  AirportState copywith(
  {
    List<AirportState> items,
    int total,
  }){
    return AirportState(items:items ??this.items ,total: total ??this.total);
  }

}
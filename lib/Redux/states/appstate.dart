import 'package:flutter/widgets.dart';
import 'airport_state.dart';
import 'general_setting_state.dart';
import 'offers_state.dart';
import 'request_state.dart';
import 'search_state.dart';
import 'share_state.dart';
import 'user_state.dart';

class AppState {
    final UserState user;
    final AirportState airoprt;
    final GeneralSettingState genertsetting;
    final OffersState offers;
    final RequestState request;
    final SearchState search;
    final ShareState share;


    AppState({
     @required this.airoprt, 
     @required this.genertsetting,
     @required  this.offers,
     @required this.request,
     @required this.search,
     @required this.share,
      @required this.user
    });

    factory AppState.initial(){
      return AppState(
        user:UserState.initial(), 
        airoprt: AirportState.initial(), 
        genertsetting: GeneralSettingState.initial(),
         offers: OffersState.initial(),
          request: RequestState.initial(),
           search: SearchState.initial(), 
           share: ShareState.initial()
           );
    }

}


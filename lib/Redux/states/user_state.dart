import 'package:driver_share/Redux/models/user_model.dart';
import 'package:flutter/foundation.dart';

class UserState{
  final List<Users> items;
  final int total;

  UserState({@required this.items,this.total});

  factory UserState.initial(){
    return UserState(
      items: List.unmodifiable(<UserState>[]),
    );
  }
  UserState copywith(
  {
    List<UserState> items,
    int total,
  }){
    return UserState(items:items ??this.items ,total: total ??this.total);
  }

}
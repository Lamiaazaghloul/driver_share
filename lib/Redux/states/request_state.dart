class RequestState{
  final List<RequestState> items;
  final int total;

  RequestState({this.items,this.total});

  factory RequestState.initial(){
    return RequestState(
      items: List.unmodifiable(<RequestState>[]),
    );
  }
  RequestState copywith(
  {
    List<RequestState> items,
    int total,
  }){
    return RequestState(items:items ??this.items ,total: total ??this.total);
  }

}
class SearchState{
  final List<SearchState> items;
  final int total;

  SearchState({this.items,this.total});

  factory SearchState.initial(){
    return SearchState(
      items: List.unmodifiable(<SearchState>[]),
    );
  }
  SearchState copywith(
  {
    List<SearchState> items,
    int total,
  }){
    return SearchState(items:items ??this.items ,total: total ??this.total);
  }

}
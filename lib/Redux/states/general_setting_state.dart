class GeneralSettingState{
  final List<GeneralSettingState> items;
  final int total;

  GeneralSettingState({this.items,this.total});

  factory GeneralSettingState.initial(){
    return GeneralSettingState(
      items: List.unmodifiable(<GeneralSettingState>[]),
    );
  }
  GeneralSettingState copywith(
  {
    List<GeneralSettingState> items,
    int total,
  }){
    return GeneralSettingState(items:items ??this.items ,total: total ??this.total);
  }

}
class ShareState{
  final List<ShareState> items;
  final int total;

  ShareState({this.items,this.total});

  factory ShareState.initial(){
    return ShareState(
      items: List.unmodifiable(<ShareState>[]),
    );
  }
  ShareState copywith(
  {
    List<ShareState> items,
    int total,
  }){
    return ShareState(items:items ??this.items ,total: total ??this.total);
  }

}
import 'package:driver_share/Globals/Tools/print.dart';
import 'package:driver_share/Globals/api/apiHelper.dart';
import 'package:driver_share/Redux/models/user_model.dart';
import 'package:driver_share/Redux/states/appstate.dart';
import 'package:redux/redux.dart';

class PushUsersAction {
  final List<Users> items;
  final int total;
  PushUsersAction(this.items, this.total);
}

class SetUsersAction {
  final List<Users> items;
  final int total;
  SetUsersAction(this.items, this.total);
}

class NewUsersAction {
  final Users user;
  NewUsersAction(this.user);
}


Future loadusers(Store<AppState> store, int convid,int skip){
   return ApiHelper.instance.fetchListItems("url,",
   params:{"convid":convid }, skip:skip)
   .then((response){
     var data =response.data.map(
       (item){
         return Users.fromJson(item);
       }
     ).toList();
     if(skip>10)
     store.dispatch(PushUsersAction(data,response.total));
     else
     store.dispatch(SetUsersAction(data,response.total));
   }).catchError((err){
     printRed(err);
   });
}


Future addUser(Store<AppState> store ,Map<String,dynamic> data)
{
  return ApiHelper.instance.postJsonData("", data,
    mapResponse: (response)=>{
    "status":response.data["status"]==true ?200:400,
    "data":response.data["data"]

  },).then((response){
    var user=Users.fromJson(response.data);
    store.dispatch(NewUsersAction(user));
    return user;
  }).catchError((err){
     printRed(err);       
  });
}
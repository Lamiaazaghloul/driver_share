import 'package:driver_share/Components/Drawer_App.dart';
import 'package:flutter/material.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';

import 'my_previous_trips.dart';
import 'my_trip.dart';



class TabsTrips extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<TabsTrips> {


  final List<Tab> tabs = <Tab>[
    Tab(text: " الرحلات الحالية"),
    Tab(text: "كل الرحلات" ),
  ];



  final List<Widget> widgetTab = <Widget>[
    new  CurrentTrips(),
    new AllTrips(),
  ];


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length:tabs.length,
        child: Scaffold(

          appBar: AppBar(
              title: Text(" الرحلات "),
              elevation: 0,
 automaticallyImplyLeading: false,

              backgroundColor: anCyan,
              centerTitle: true,

              // actions: <Widget>[
              //   new Padding(padding: EdgeInsets.symmetric(horizontal: 20) ,
              //     child: CircleAvatar(backgroundColor: anwhite, maxRadius: 15,
              //       child: Icon(Icons.arrow_forward_ios, size: 20,),),)
              // ],

              bottom: PreferredSize(
                  child: Container(color: anwhite,
                    child: TabBar(
                      isScrollable: false,
                      tabs: tabs,
                      unselectedLabelColor: Colors.grey,
                      labelColor: Colors.black,
                      indicatorColor: Colors.amber,
                      
                    ),), preferredSize: Size.fromHeight(50))
          ),

 //---------endDrawer---------------------------------
        endDrawer: DrawerApp(),
          body: TabBarView(
              children: widgetTab
          ),

        ),
      
    );
  }
}
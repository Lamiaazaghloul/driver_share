
import 'package:driver_share/Components/Drawer_App.dart';
import 'package:driver_share/Pages/prices/airport_prices.dart';
import 'package:driver_share/Pages/prices/prices_rent_car.dart';
import 'package:driver_share/Pages/prices/share_price.dart';
import 'package:flutter/material.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';



class PageTabBarView extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageTabBarView> {


  final List<Tab> tabs = <Tab>[
    Tab(text: "مشاركة لسيارة", icon: Icon(Icons.directions_car,color: Colors.grey,),),
    Tab(text: " مشاركة للمطار", icon: Icon(Icons.location_on ,color: Colors.grey,),),
    Tab(text: "تأجير سيارة", icon: Icon(Icons.calendar_today ,color: Colors.grey,),),

  ];



  final List<Widget> widgetTab = <Widget>[
    new PageSharePrice(),
    new PageAirportPrice(),
    new PriceRentCar(),

    
    // new PageCarForRent()
  ];


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length:tabs.length,
        child: Scaffold(

          appBar: AppBar(
            title: Image.asset("asset/Logo1.png" ,height: 40,),
            // leading: IconButton(icon: Icon(Icons.arrow_back_ios , size: 30,), onPressed: (){ Navigator.pop(context);}),
            elevation: 0,
            backgroundColor: anCyan,
            centerTitle: true,
              automaticallyImplyLeading: false,

            // actions: <Widget>[
              leading: IconButton(icon: Icon(Icons.history , size: 30,), onPressed: (){}),
              // new Padding(padding: EdgeInsets.symmetric(horizontal: 20) ,
              // child: CircleAvatar(backgroundColor: anwhite, maxRadius: 15,
              // child: Icon(Icons.arrow_forward_ios, size: 20,),),)
            // ],

            bottom: PreferredSize(
              child: Container(
                color: anwhite,
              child: TabBar(
              isScrollable: false,
              tabs: tabs,
              unselectedLabelColor: Colors.grey,
              labelColor: Colors.black,
              indicatorColor: Colors.amber,
            ),), preferredSize: Size.fromHeight(80))
          ),

 //---------endDrawer---------------------------------
        endDrawer: DrawerApp(),
          body:  TabBarView(
              children: widgetTab
          ),

        
      
    ));
  }
}

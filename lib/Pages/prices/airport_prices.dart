
import 'package:driver_share/Components/map.dart';
import 'package:driver_share/Globals/Tools/print.dart';
import 'package:flutter/material.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:toast/toast.dart';
import '../../translations.dart';

class PageAirportPrice extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageAirportPrice> {

  void funactionSearch(){
    Toast.show("Start Search", context);
  }



  TextEditingController controllerFrom = new TextEditingController();
  TextEditingController controllerTo = new TextEditingController();
  TextEditingController controllerDate = new TextEditingController();
  TextEditingController controllerPeople = new TextEditingController();
  TextEditingController controllerCar = new TextEditingController();

_onBasicAlertPressed(context) {
    Alert(
        context: context,
        title: "حدد مكان الوصول",
        content: Column(
          children: <Widget>[
             new Container(
                     height: 400,
                     width: 300,
                    child:new Column(children: <Widget>[
                     
                      new Container(
  height: 50,
  child: 
                 myTextFieldMaterial(horizontal: 3, Radius: 4 , elevation: 0.5,controllers: controllerFrom,
                  prefixIcon: Icons.airplanemode_active , HintText:Translations.of(context).text('from_airport'),textInputType: TextInputType.text),
              // 
              
),
//  new Divider(),
prefix0.SizedBox(height: 5,),
 new Container(
                     height: 320,
                     width: MediaQuery.of(context).size.width,
                     child: GetLcationFromMap(),
                   ),
                    ],)
                  
                  )
          ],
        ),
        ).show();
  }


 
functionLocation()
{
  printGreen("data");
    showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    content: 
                     new Container(
                     height: 450,
                     width: 300,
                    child:new Column(children: <Widget>[
                      new Text("حدد مكان الوصول"),
                      new Divider(),
                      new Container(
  height: 50,
  child: 
                 myTextFieldMaterial(horizontal: 3, Radius: 4 , elevation: 0.5,controllers: controllerFrom,
                  prefixIcon: Icons.airplanemode_active , HintText:Translations.of(context).text('from_airport'),textInputType: TextInputType.text),
              // 
              
),
//  new Divider(),
prefix0.SizedBox(height: 5,),
 new Container(
                     height: 330,
                     width: MediaQuery.of(context).size.width,
                     child: GetLcationFromMap(),
                   ),
                  

                    ],)
                  
                  ));
                });
         
  //  Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => LeavingLocation()),
  // );
}
 
  @override
  Widget build(BuildContext context) {
    return

         ListView(
          children: <Widget>[
           Column(children: <Widget>[

              Row(
              children: <Widget>[
                 SizedBox(width: 15,),
                 new Icon( Icons.location_on,color:Colors.grey,size: 25,) ,
                 myButtonLoction(
                   colorButton:Colors.transparent,colorText: Colors.grey,
                textButton:Translations.of(context).text('from_airport')  , onBtnclicked: (){_onBasicAlertPressed(context);} , fontSize: 16)
                     ,
                  
              ],
            ),
                   
            
              new Divider(),
  Row(
              children: <Widget>[
                 SizedBox(width: 15,),
                 new Icon( Icons.location_on,color:Colors.grey,size: 25,) ,
                 myButtonLoction(
                   colorButton:Colors.transparent,colorText: Colors.grey,
                textButton:Translations.of(context).text('arriver_place') , onBtnclicked: (){functionLocation();} , fontSize: 16)
                     ,
                  
              ],
            ),     
              // myTextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerFrom,
              //     prefixIcon: Icons.airplanemode_active , HintText:Translations.of(context).text('from_airport'),textInputType: TextInputType.text),
              // new Divider(),

              // myTextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerTo,
              //     prefixIcon: Icons.location_on , HintText: Translations.of(context).text('arriver_place'),textInputType: TextInputType.text),
              new Divider(),


              // myTextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerDate,
              //     prefixIcon: FontAwesomeIcons.calendarAlt ,
              //      HintText:Translations.of(context).text('tripdate'),textInputType: TextInputType.text),
              // new Divider(),


              // myTextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerPeople,
              //     prefixIcon: FontAwesomeIcons.users , HintText: Translations.of(context).text('person_number'),textInputType: TextInputType.text),
              // new Divider(),


              myTextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: controllerCar,
                  prefixIcon: Icons.directions_car , HintText: Translations.of(context).text('Type_car'),textInputType: TextInputType.text),
              new Divider(),
   myTextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: controllerCar,
                  prefixIcon: Icons.monetization_on , HintText: Translations.of(context).text('price'),textInputType: TextInputType.text),
              new Divider(),

 new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Row(children: <Widget>[
                    SizedBox(width: 20),
                      new Icon(Icons.straighten , color: Colors.grey[500],),
                       SizedBox(width: 10),
                      new Text(Translations.of(context).text('air'), style: TextStyle(fontSize: 17 , color: angrey),),
                    ],),
                    new Checkbox(value: value1, onChanged: _onChanged )
                  ],
                ),
  new Divider(),
              SizedBox(height: 20,),
              myButton(horizontal: 20 , textButton:  Translations.of(context).text('apply') , onBtnclicked: (){funactionSearch();} ,colorButton: anCyan , fontSize: 18)

            ],),
          ],
        



      
    );
  }
  //====== Function محطات   =================
  bool value1 = true;
  void _onChanged(bool value){
    setState(() {
      value1 = value ;
      if(value == true){
        Toast.show(" مكيف  ", context);
      }
      else{  Toast.show("غير مكيف  ", context); }
    });
  }
}
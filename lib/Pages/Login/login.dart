
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:driver_share/Components/NavigationBar.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:driver_share/Pages/forgot_password/forgotpassword.dart';
import 'package:driver_share/Pages/register/step_one.dart';

import 'package:toast/toast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../translations.dart';

class Login extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<Login> {

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";

  TextEditingController phoneNumber  = new TextEditingController();
  TextEditingController secondName  = new TextEditingController();
  TextEditingController email  = new TextEditingController();

  void forgotPassword(){
    Toast.show("هل نسيت كلمة السر ", context);
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PageResetPassword()),
  );
  }

  void functionLogIn(){
    Toast.show("Log In", context);
     Navigator.pushReplacement(
    context,
    MaterialPageRoute(builder: (context) => NavigationBar()),
  );
  }

  void functionNewAccount(){
    // Toast.show("لا تمتلك حساب ؟ سجل الان", context);
    Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => RegisterOne()),
  );
  }

  void functionfacebook(){
    Toast.show("facebook", context);
  }

  void functionTwitter(){
    Toast.show("twitter", context);
  }

  

  @override
  Widget build(BuildContext context) {
    return  new Scaffold(
        backgroundColor: anCyan,
        body: ListView(
          children: <Widget>[
            // Header top=========================================
            new Stack(
              overflow: Overflow.visible,
              children: <Widget>[

                new Container(
                    height: 200, width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: anwhite,
                        image: DecorationImage(
                            colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.1), BlendMode.dstATop),
                            fit: BoxFit.cover,
                            image: NetworkImage(imageURL))
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Align(alignment: Alignment.center, child: Image.asset("asset/Logo2.png"),),
                    )
                ),

                Positioned(
                    bottom: -60, left: MediaQuery.of(context).size.width /2 -55,
                    child: myIconInContainer(
                      color1: anCyan ,color2: Colors.white, heightWidth: 120,
                      widget: Icon(Icons.account_circle ,size: 110 ,color: anCyan,) ,
                    )),

              ],
            ),
            SizedBox(height: 80),

            //  TextField phone  =========================================
            myTextFieldMaterial(
                    Radius: 10 ,horizontal:30 , elevation: 0,textInputType: TextInputType.phone ,
                     vertical: 2,
                    prefixIcon: Icons.email ,
                    HintText: Translations.of(context).text('phone_number'),
                     controllers: phoneNumber)
            ,

SizedBox(height: 5),
            //  TextField  Second name =========================================
            myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.text , vertical: 4, obscureText: true,
                    prefixIcon: Icons.lock ,HintText: Translations.of(context).text('password') ,  controllers: secondName)
            ,
            Padding(
              padding: const EdgeInsets.only(right: 30,left: 30),
              child:InkWell(onTap: (){forgotPassword();},
              child: new Text(Translations.of(context).text('forgot_password') ,style: TextStyle(color: anwhite , fontSize: 17),))
            ),
            // Button next =========================================
            SizedBox(height: 10,),
            myButton(
                onBtnclicked: (){functionLogIn();} ,
                horizontal: 70 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
                textButton:Translations.of(context).text('login')  , fontSize: 17
            ),
            SizedBox(height:20 ,),

            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

InkWell(onTap: (){functionNewAccount();},
              child: new Text(Translations.of(context).text('ceare_new_account') ,style: TextStyle(color: anwhite , fontSize: 17),))
                // new Text("انشاء حساب جديد" ,style: TextStyle(color:anwhite ),),

               , SizedBox(width: 5),
                myIconInContainer(
                    onTap: (){print("FaceBook");},color1: Colors.blue[800] ,
                    widget: Icon(FontAwesomeIcons.facebookF, color: Colors.white,)
                ),

                SizedBox(width: 5),
                myIconInContainer(
                    onTap: (){print("FaceBook");},color1: Colors.blue ,
                    widget: Icon(FontAwesomeIcons.twitter, color: Colors.white,)
                ),

              ],
            ),

          ],
        ),



      
    );
  }
}
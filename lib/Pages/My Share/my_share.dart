
import 'package:driver_share/Components/Drawer_App.dart';
import 'package:flutter/material.dart';
import 'package:driver_share/Components/Connect.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';

import '../../translations.dart';


class Myposts extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<Myposts> {


  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";
  String imageMan = "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500";


  @override
  Widget build(BuildContext context) {
    return  new Scaffold(

        // ==AppBar=========================================
        appBar: AppBar(
          title: Text(Translations.of(context).text('mySahre') ,),
          centerTitle: true,
          elevation: 0,
          backgroundColor: anCyan,
           automaticallyImplyLeading: false,
        ),


  //---------endDrawer---------------------------------
        endDrawer: DrawerApp(),
        //==body============================================
        body: ListView(
          children: <Widget>[

            myOffersList(
              book:Translations.of(context).text('accept') ,
              colorType:Colors.green,
              offerName:"طلب سيارة",
              onTap: (){} , ),
            myOffersList(onTap: (){} ,
               book:Translations.of(context).text('reject') ,
               colorType: Colors.red,
               offerName:"تأجير سيارة",
                ),
            myOffersList(onTap: (){} , 
               book:Translations.of(context).text('pending') ,
               colorType: Colors.yellow,
               offerName:"طلب سائق",
               ),
            myOffersList(onTap: (){} ,
               book:Translations.of(context).text('accept') ,
               colorType:Colors.green,
               offerName:" رحلة",
               ),
            myOffersList(onTap: (){} ,
               book:Translations.of(context).text('pending') ,
               colorType:Colors.yellow,
                ),

            myOffersList(onTap: (){} , 
               book:Translations.of(context).text('reject') ,
               colorType:Colors.red,
               ),

          ],
        







     
    ));}
}
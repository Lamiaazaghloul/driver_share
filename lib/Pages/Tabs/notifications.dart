
import 'package:driver_share/Components/Drawer_App.dart';
import 'package:flutter/material.dart';
import 'package:driver_share/Components/Connect.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';

import 'package:toast/toast.dart';

import '../../translations.dart';

class MyNotifcation extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<MyNotifcation> {


  void functionArrange(){
    Toast.show("Arrange", context);
  }

  void functionFilter(){
    Toast.show("Filter", context);
  }

  void functionButtonMenu(){
    Toast.show("ButtonMenu", context);
  }

  void functionButtonback(){
    Toast.show("Buttonback", context);
  }

  void functionSAR(){
    Toast.show("SAR", context);
  }

  TextEditingController searchController = new TextEditingController();

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";
  String imageMan = "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500";


  bool value4 = false;
  void onChangedValue4( bool value){
    setState(() {
      value4 = value;
      if(value4 == true){
        Toast.show("ON", context);
      }else{
        Toast.show("OFF", context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return  new Scaffold(

        //===AppBar=============================================
        appBar: AppBar(
          elevation: 0,
          backgroundColor: anCyan,
          centerTitle: true,
 automaticallyImplyLeading: false,
          //----------TextField Search -----------------
          title:Text(Translations.of(context).text('notificatios')
          ),
         
        ),

         //---------endDrawer---------------------------------
        endDrawer: DrawerApp(),
        body: ListView(children: <Widget>[
          myNotificatioList(
            onTap: (){} , ),
          myNotificatioList(onTap: (){} ,
            ),
          myNotificatioList(onTap: (){} ,
            ),
          myNotificatioList(onTap: (){} ,
            ),
          myNotificatioList(onTap: (){} ,
            ),
             myNotificatioList(onTap: (){} ,
            ),
             myNotificatioList(onTap: (){} ,
            ),
          myNotificatioList(onTap: (){} ,
            ),
        ],),



    
    );
  }
}
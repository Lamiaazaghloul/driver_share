
import 'package:driver_share/Components/Drawer_App.dart';
import 'package:driver_share/Globals/Tools/textField.dart';
import 'package:driver_share/Pages/Tabs/orders/order_details.dart';
import 'package:flutter/cupertino.dart' as prefix0;
import 'package:flutter/material.dart';
import 'package:driver_share/Components/Connect.dart';
import 'package:driver_share/Components/Filter.dart';
import 'package:driver_share/Components/SortbBy.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:toast/toast.dart';

import '../../../translations.dart';
import '../notifications.dart';

class OrdersPage extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<OrdersPage> {

double result = 0.0;
int radioValue;
void handleRadioValueChange(int value) {
    setState(() {
      radioValue = value;
  
      switch (radioValue) {
        case 0:
          // _result = 
          break;
        case 1:
          // _result = ...
          break;
        case 2:
          // _result = ...
          break;
      }
    });
  }


  void functionArrange(context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
          return Container(
            child: new Wrap(
            children: <Widget>[



new ListTile(
            leading: new Icon(Icons.list),
            title: new Text('ترتيب حسب'),
            onTap: () => {}          
          ),
          


           new Divider(),
              //Nearby
new ListTile(
            leading: new Icon(Icons.location_on),
            title: new Text('الأقرب'),
            onTap: () => {}          
          ),
          //price
          new ListTile(
            leading: new Icon(Icons.attach_money),
            title: new Text('الاقل سعر'),
            onTap: () => {},          
          ),

                        //car new
new ListTile(
            leading: new Icon(Icons.local_car_wash),
            title: new Text('نوع السيارة الاحدث'),
            onTap: () => {}          
          ),
          //car old
          new ListTile(
            leading: new Icon(Icons.directions_car),
            title: new Text('نوع السيارة الاقدم'),
            onTap: () => {},          
          ),

                        //car smal
new ListTile(
            leading: new Icon(Icons.add_circle),
            title: new Text('حجم السيارة الاكبر'),
            onTap: () => {}          
          ),
          //car big
          new ListTile(
            leading: new Icon(Icons.remove_circle),
            title: new Text('حجم السارة الاصغر'),
            onTap: () => {},          
          ),

            ],
         ),
          );
      }
    );
}

int currVal = 1;
  String currText = '';

  List<GroupModel> group = [
    GroupModel(
      text: "Flutter.dev",
      index: 1,
    ),
    GroupModel(
      text: "Inducesmile.com",
      index: 2,
    ),
    GroupModel(
      text: "Google.com",
      index: 3,
    ),
    GroupModel(
      text: "Yahoo.com",
      index: 4,
    ),
  ];

   void functionFilter(){
    showModalBottomSheet(context: context, builder: (BuildContext context){
      return new Filter();
    });
  }


void funReservation(){
  _displayDialog(context);
  //  Navigator.push(
  //     context,
  //     MaterialPageRoute(builder: (context) => ResDetails()),
  //   );
}

void funTripDetails(){
  
   Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => OrederDetalis()),
    );
}

void functionSortbBy(){
    showModalBottomSheet(context: context, builder: (BuildContext context){
      return new SortbBy();
    });
  }
  
   void functionButtonMenu(){
   prefix0.showCupertinoModalPopup(context: context, builder: (BuildContext context){
      return Drawer(
        
            child: ListView(
              children: <Widget>[

                new DrawerHeader(
                child: Text(Translations.of(context).text('Select_your_search_filter_options_as_desired'),style: TextStyle(fontSize: 20 , color: angrey),
                textAlign: TextAlign.center),
                ),

                //============================ مكان الانطاق  ===
                Padding(
                  padding: const EdgeInsets.only( left: 10 , right: 10 ),
                  child: new Text(Translations.of(context).text('Starting_place'), style: TextStyle(color: angrey),),
                ),

                myTextFieldMaterial( prefixIcon: Icons.my_location , elevation: 0 ,
                HintText: Translations.of(context).text('Click_here_to_locate_the_tweet'), horizontal: 10 ,
                ),

                //============================ مكان الوصول  ===
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only( left: 10 , right: 10 ),
                  child: new Text(Translations.of(context).text('END_place'), style: TextStyle(color: angrey),),
                ),

                myTextFieldMaterial( prefixIcon: Icons.location_on , elevation: 0 ,
                HintText: Translations.of(context).text('Click_here_to_locate_the_destination'), horizontal: 10 ,
                ),

                //============================ مكان الانطاق  ===
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only( left: 10 , right: 10 ),
                  child: new Text(Translations.of(context).text('time_leavel') , style: TextStyle(color: angrey),),
                ),

                myTextFieldMaterial( prefixIcon: Icons.developer_board , elevation: 0 ,
                HintText: "الثلاثاء 5/5/1991" , horizontal: 10 ,
                ),

              ],
            
          ),
        );
    });
  }


  void functionButtonNotify(){
//    Toast.show("Buttonback", context);
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyNotifcation()),);
              
       
  }

  void functionSAR(){
    Toast.show("SAR", context);
  }

  TextEditingController searchController = new TextEditingController();

  String imageURL = "https://icon-library.net/images/auto-icon/auto-icon-6.jpg";
  String imageMan = "https://icon-library.net/images/auto-icon/auto-icon-6.jpg";


  bool value4 = false;
  void onChangedValue4( bool value){
    setState(() {
      value4 = value;
      if(value4 == true){
        Toast.show("ON", context);
      }else{
        Toast.show("OFF", context);
      }
    });
  }
   Widget appBarTitle = InkWell( onTap: (){}, child:Text("الطلبات"));
   
   Icon actionIcon = new Icon(Icons.search,);

TextEditingController textFieldController = TextEditingController();

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            
             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text("يمكنك ان تقدم عرضك علي هذا الطلب",style: TextStyle(fontSize: 15,color: anCyan),),
            content:myTextFieldMaterial(
                    Radius: 1 ,horizontal: 0 , elevation: 1,textInputType: TextInputType.number ,
                    prefixIcon: Icons.monetization_on ,HintText: "مثال/50جنية")
            ,

            actions: <Widget>[
              new FlatButton(
                child: new Text('قدم',style: TextStyle(fontSize: 15,color: anCyan)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        //===AppBar=============================================
           appBar: AppBar(
          backgroundColor:anCyan,
          elevation: 0,
          centerTitle: true,
          title:appBarTitle,
           automaticallyImplyLeading: false,
           
          // leading:  Padding(
          //     padding: const EdgeInsets.all(8.0),
          //     child: InkWell(onTap: (){_FunctionButtonNotify();},
          //       child: new Container(
          //         width: 15,
          //         child:CircleAvatar(backgroundColor: anwhite, child: 
          //       Image.asset("asset/notification.png",width: 25,),),),
          // )),
         
// actions: <Widget>[
          leading:IconButton(icon: actionIcon,onPressed:(){
            setState(() {
              if ( this.actionIcon.icon == Icons.search){
                this.actionIcon = new Icon(Icons.close);
                this.appBarTitle = new TextField(
                  style: new TextStyle(color: Colors.white),
                  decoration: new InputDecoration(
                    prefixIcon: new Icon(Icons.search,color: Colors.white),
                    hintText: "ابحث هنا",
                    hintStyle: new TextStyle(color: Colors.white54),
                  ),
                  textAlign: TextAlign.center,
                  cursorColor: Colors.amber,
                  textInputAction: TextInputAction.search,
                  onEditingComplete: (){ 
                    // Function_EditingComplete();
                    },
                );
              }
              else {
                this.actionIcon = new Icon(Icons.search);
                this.appBarTitle = InkWell( onTap: (){
                  // Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()),);
                  },
                    child: Center(child:new Text(Translations.of(context).text('orders'))));
              }
            });
          } ,),
      //  ],
        ),          

//           //----------bottom -----------------
//           bottom: PreferredSize(
//               child: IntrinsicHeight(
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: <Widget>[
//  GestureDetector(onTap: (){_FunctionSortbBy();},
//                       child: new Row(
//                         children: <Widget>[
//                           Text(Translations.of(context).text('sort_by') ,style: TextStyle(color: anwhite),), SizedBox(width: 10),
//                           Icon(FontAwesomeIcons.alignRight,color: anwhite , size: 15,)
//                         ],
//                       ),
//                     ),
//                     VerticalDivider(),
//                     GestureDetector(onTap: (){_FunctionFilter();},
//                       child: new Row(
//                         children: <Widget>[
//                           Text(Translations.of(context).text('fillter') ,style: TextStyle(color: anwhite),),SizedBox(width: 10),
//                           Icon(FontAwesomeIcons.sortAmountDownAlt,color: anwhite , size: 15)
//                         ],
//                       ),
//                     ),

//                     VerticalDivider(),
//                     IconButton(icon: Icon(Icons.menu,color: anwhite), onPressed:
//                      (){
//                       //  _FunctionButtonMenu();
//                        }),

                   

//                   ],
//                 ),
//               ), preferredSize: Size.fromHeight(40)),
//         ),
       
       
  //---------endDrawer---------------------------------
        endDrawer: DrawerApp(),
        body: ListView(children: <Widget>[
        SizedBox(height:10),
          myOrdersList(
            takeUserFromHome: Translations.of(context).text('take_home'),
            rail: Translations.of(context).text('real'),
             book: Translations.of(context).text('make_offer'),
            details:(){
                    funTripDetails();},
            onTap: (){
               funReservation();
          } , 
          ),

             myOrdersList(
            takeUserFromHome: Translations.of(context).text('take_home'),
            rail: Translations.of(context).text('real'),
             book: Translations.of(context).text('make_offer'),
            details:(){
                    funTripDetails();},
            onTap: (){
               funReservation();
          } , 
          ),

             myOrdersList(
            takeUserFromHome: Translations.of(context).text('take_home'),
            rail: Translations.of(context).text('real'),
             book: Translations.of(context).text('make_offer'),
            details:(){
                    funTripDetails();},
            onTap: (){
               funReservation();
          } , 
          ),

             myOrdersList(
            takeUserFromHome: Translations.of(context).text('take_home'),
            rail: Translations.of(context).text('real'),
             book: Translations.of(context).text('make_offer'),
            details:(){
                    funTripDetails();},
            onTap: (){
               funReservation();
          } , 
          ),

             myOrdersList(
            takeUserFromHome: Translations.of(context).text('take_home'),
            rail: Translations.of(context).text('real'),
             book: Translations.of(context).text('make_offer'),
            details:(){
                    funTripDetails();},
            onTap: (){
               funReservation();
          } , 
          ),

             myOrdersList(
            takeUserFromHome: Translations.of(context).text('take_home'),
            rail: Translations.of(context).text('real'),
            book: Translations.of(context).text('make_offer'),
            details:(){
                    funTripDetails();},
            onTap: (){
               funReservation();
          } , 
          ),

           ],),



      
    );
  }
}
class GroupModel {
  String text;
  int index;
  GroupModel({this.text, this.index});
}
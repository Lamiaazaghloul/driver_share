
import 'package:flutter/material.dart';
import 'package:driver_share/Components/Connect.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:toast/toast.dart';

import '../../../translations.dart';



class OrederDetalis extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<OrederDetalis> {

  // void functionArrange(){
  //   Toast.show("Arrange", context);
  // }

  // String imageMan = 'https://cdn.pixabay.com/photo/2015/01/08/18/29/entrepreneur-593358__480.jpg';

  // void functionProblem(){
  //   Toast.show("الابلاغ عن مشكلة", context);
  // }

  @override
  Widget build(BuildContext context) {
    return  new Scaffold(
        backgroundColor: Colors.white,
        //==AppBar========================================
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: Text(Translations.of(context).text('order_details') ),
          backgroundColor: anCyan,
          // leading: IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: (){}),
        ),


        //==body========================================
        body: ListView(
          children: <Widget>[

            // //------------- صور السيارة---------
            // new Container( height: 200,
            // decoration: BoxDecoration(
            //   image: DecorationImage(
            //     fit: BoxFit.cover,
            //     image: AssetImage("asset/car_cover.png"))
            // ),
            // ),

            //------------- معلومات عن السائق ---------
            myOrderDetails(
              book:Translations.of(context).text('book')
              
           
            ),

            //------------- تفاصيل السائق ---------
            new Container(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(Translations.of(context).text('driver_details') ),
                    new Text("لكن كتعريف علمى او كما يعرفه موقع المطورين هو عباره عن سلوك او واجهة مستخدم يمكن تضمينه بداخل Activity  اى انه عباره عن شىء مثل الاكتيتفى لكن يمكن وضعه داخل أكتيتفى أخر  حيث فى الوضع العادى لا يمكن وضع Activity داخل Activity أخر او وضع اثنين من الـ Activities فى شاشة واحدة  أكثر تنظيما" ,
                      style: TextStyle(color: Colors.grey), textAlign: TextAlign.right,),

                    //------------- من جدة الي الرياض ---------
                    new Divider(height: 15,),
                    mySubOreserDetails()

                  ],
                ),
              ),
            ),
            new Container(
              padding: const EdgeInsets.only(left:10,right: 10),
            child:new Text("قدم عرضك::",style:TextStyle(fontSize: 20),),
            ),
         new Container(
           color: Colors.white,
             child:myTextField(
               horizontal: 120,
               hintText: "مثال/50 جنيه",
              textAlign: TextAlign.center,
               vertical: 2,
               Radius: 5,
             )
             ),
            //------------زر مراسلة السائق  --------
            SizedBox(height: 20),
            myButtonIcon(
                onPressed: (){}, horizontal: 100 , textButton:Translations.of(context).text('send'),
                radiusButton: 5  , colorButton: anCyan , icon: Icons.message
            ),
            SizedBox(height: 40),
          ],
        ),


     
    );
  }
}
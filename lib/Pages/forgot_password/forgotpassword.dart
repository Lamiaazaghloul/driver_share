import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:driver_share/Pages/forgot_password/resetpassword.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:toast/toast.dart';

import '../../translations.dart';

class PageResetPassword extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageResetPassword> {
  String imageURL =
      "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";

  TextEditingController emailPassword = new TextEditingController();

  void forgotPassword() {
    Toast.show("هل نسيت كلمة السر ", context);
  }

  void resetPassword() {
    Toast.show(" Reset Password ", context);
      Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => ResetPassword()),
  );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: anCyan,
        body: ListView(
          children: <Widget>[
            // Header top=========================================
            new Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                new Container(
                    height: 250,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: anwhite,
                        image: DecorationImage(
                            colorFilter: new ColorFilter.mode(
                                Colors.white.withOpacity(0.1),
                                BlendMode.dstATop),
                            fit: BoxFit.cover,
                            image: NetworkImage(imageURL))),
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Image.asset("asset/Logo2.png"),
                      ),
                    )),
                Positioned(
                    bottom: -60,
                    left: MediaQuery.of(context).size.width / 2 - 55,
                    child: myIconInContainer(
                      color1: anCyan,
                      color2: Colors.white,
                      heightWidth: 120,
                      widget: Icon(
                        Icons.account_circle,
                        size: 110,
                        color: anCyan,
                      ),
                    )),
              ],
            ),
            SizedBox(height: 60),

            Padding(
              padding: const EdgeInsets.all(20),
              child: Align(
                  alignment: Alignment.center,
                  child: InkWell(
                      onTap: () {
                        forgotPassword();
                      },
                      child: new Text(
                        Translations.of(context).text('enter_yoy_email'),
                        style: TextStyle(color: anwhite, fontSize: 18),
                        textAlign: TextAlign.center,
                      ))),
            ),

            //  TextField phone  =========================================
           myTextFieldMaterial(
                    Radius: 5,
                    horizontal: 20,
                    elevation: 0,
                    textInputType: TextInputType.phone,
                    vertical: 4,
                    prefixIcon: Icons.email,
                    HintText: Translations.of(context).text('email'),
                    controllers: emailPassword),

            // Button next =========================================
            SizedBox(
              height: 20,
            ),
            myButton(
                onBtnclicked: () {
                  resetPassword();
                },
                horizontal: 80,
                radiusButton: 40,
                colorButton: anwhite,
                colorText: Colors.grey,
                heightButton: 60,
                textButton: Translations.of(context).text('retern_password'),
                fontSize: 15),
          ],
        ),
    
    );
  }
}


import 'package:flutter/material.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:driver_share/Pages/login/login.dart';

import '../../translations.dart';


class PageSendCodeActive extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageSendCodeActive> {
  @override
  Widget build(BuildContext context) {
    return  new Scaffold(
        backgroundColor: anCyan,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            myIconInContainer(color1: anwhite , heightWidth: 140 , widget: Icon(Icons.check , size: 100 ,color: Colors.green,)),

            Padding(
              padding: const EdgeInsets.all(20),
              child: Align(alignment: Alignment.center,
                  child: new Text(Translations.of(context).text('succes_returnpassword'),style: TextStyle(color: anwhite , fontSize: 18),textAlign: TextAlign.center,)
              ),
            ),


            SizedBox(height: 20),
            myButton(
                onBtnclicked: (){
                   Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Login()),
  );
                } ,
                horizontal: 100 , radiusButton: 40 , colorButton: anwhite , colorText: Colors.grey, heightButton: 60,
                textButton: Translations.of(context).text('login') , fontSize: 17
            ),



          ],
        ),),



     
    );
  }
}

import 'package:flutter/material.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:driver_share/Pages/login/login.dart';

import '../../translations.dart';



class PageActivationCode extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageActivationCode> {


  //  ============== ============================اكشن التفعيل
  void functionButtonActive(){
    print("متابعة ");
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Login()),
  );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: anCyan,

        body: ListView(
          children: <Widget>[

            // image Logo==========================================
            new Container(
              padding: EdgeInsets.only(top: 130 , left: 50 , right: 50),
              child: Image.asset("asset/Logo1.png",height: 85,),
            ),

            // text phone Number ======================================
            SizedBox(height: 30,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Align( alignment: Alignment.center,
                  child: new Text(Translations.of(context).text('code_send'),
                    style: TextStyle(fontSize: 20 , color: anwhite ),textAlign: TextAlign.center,)
              ),
            ),

            // text TextField =========================================
            SizedBox(height: 30,),
             myTextFieldMaterial(
                    Radius: 1 ,horizontal: 50 , elevation: 0,textInputType: TextInputType.number ,
                    prefixIcon: Icons.done ,HintText: Translations.of(context).text('activation_code'))
            ,


            // text TextField =========================================
            SizedBox(height: 30,),
            myButton(
                onBtnclicked: (){functionButtonActive();} ,
                horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
                textButton: Translations.of(context).text('Activation') , fontSize: 20
            )

          ],
        ),


      
    );
  }
}
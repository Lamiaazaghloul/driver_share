

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:driver_share/Pages/register/step_four.dart';

import '../../translations.dart';



class RegisterThree extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<RegisterThree> {

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";

  TextEditingController carNumber  = new TextEditingController();
  TextEditingController endDate  = new TextEditingController();
  TextEditingController licensePlateNumber  = new TextEditingController();
  TextEditingController carType = new TextEditingController();
  TextEditingController carCategory = new TextEditingController();

  void functionButtonNext(){
   Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => RegisterFour()),
  );  // Toast.show("التالي", context);

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: anCyan,


        body: ListView(
          children: <Widget>[

            // Header top=========================================
            new Stack(
              overflow: Overflow.visible,
              children: <Widget>[

                new Container(
                    height: 200, width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: anwhite,
                        image: DecorationImage(
                            colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.1), BlendMode.dstATop),
                            fit: BoxFit.cover,
                            image: NetworkImage(imageURL))
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Align(alignment: Alignment.center, child: Image.asset("asset/Logo2.png",),),
                    )
                ),
              ],
            ),
            SizedBox(height: 20),

            //  TextField رقــــــم العـــــربة   =========================================
           myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.number , vertical: 3,
                    prefixIcon: Icons.phone_android ,HintText:Translations.of(context).text('car__number') , controllers: carNumber)
            ,


            //  TextField  تاريخ إنتهاء الرخصة =========================================
            myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.datetime , vertical: 3,
                    prefixIcon: Icons.account_circle ,HintText:Translations.of(context).text('Expiry_date_of_license') ,  controllers: endDate)
            ,


            //  TextField رقم لوحة السيارة =========================================
             myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.number , vertical: 3,
                    prefixIcon: Icons.email ,HintText:Translations.of(context).text('plate_number')  , controllers: licensePlateNumber)
            ,


            //  TextField نوع السيارة  =========================================
           myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.text , vertical: 3,
                    prefixIcon: Icons.email ,HintText: Translations.of(context).text('Type_car') , controllers: carType)
           ,


            //  TextField  فئة السيارة =========================================
            myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.text , vertical: 3,
                    prefixIcon: Icons.email ,HintText: Translations.of(context).text('category_car') , controllers: carCategory)
            ,


            // Button next =========================================
            SizedBox(height: 30,),
            myButton(
                onBtnclicked: (){functionButtonNext();} ,
                horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
                textButton: Translations.of(context).text('next')  , fontSize: 18
            )

          ],
        ),



      
    );  }
}


import 'package:flutter/material.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:driver_share/Pages/register/step_two.dart';

import '../../translations.dart';


class RegisterOne extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<RegisterOne> {


  //  ============== ============================اكشن التفعيل
  void functionButtonActive(){
    print("متابعة ");
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => RegisterTwo()),
  );
  }

  @override
  Widget build(BuildContext context) {
    return  new Scaffold(
        backgroundColor: anCyan,

        body: ListView(
          children: <Widget>[

            // image Logo==========================================
            new Container(
              padding: EdgeInsets.only(top: 150 , left: 50 , right: 50),
              child: Image.asset("asset/Logo1.png",height: 85,),
            ),



            // text TextField =========================================
            SizedBox(height: 45,),
             myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.text ,
                    prefixIcon: Icons.account_circle ,HintText: Translations.of(context).text('first_name'))
            ,


            // text TextField =========================================
            SizedBox(height: 15,),
            myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.number ,
                    prefixIcon: Icons.account_balance_wallet ,HintText:Translations.of(context).text('ID_Number'))
            ,


            // text TextField =========================================
            SizedBox(height: 30,),
            myButton(
                onBtnclicked: (){functionButtonActive();} ,
                horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
                textButton: Translations.of(context).text('next') , fontSize: 18
            )

          ],
        ),


   
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:driver_share/Pages/register/step_three.dart';

import '../../translations.dart';



class RegisterTwo extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<RegisterTwo> {

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";

  TextEditingController phoneNumber  = new TextEditingController();
  TextEditingController secondName  = new TextEditingController();
  TextEditingController email  = new TextEditingController();

  void functionButtonNext(){
    // Toast.show("التالي", context);
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => RegisterThree()),
  );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return  new Scaffold(
        // backgroundColor: anCyan,


        body: Stack(
        children: <Widget>[
          Center(
            child: new Image.asset(
              'asset/BG.png',
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            )),
            ListView(
          children: <Widget>[

            // Header top=========================================
            new Stack(
              overflow: Overflow.visible,
              children: <Widget>[

                new Container(
                    height: 200, width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: anwhite,
                        image: DecorationImage(
                            colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.1), BlendMode.dstATop),
                            fit: BoxFit.cover,
                            image: NetworkImage(imageURL))
                            //  image:AssetImage("asset/BG2.png")
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Align(alignment: Alignment.center, child: Image.asset("asset/Logo2.png"),),
                    )
                ),

                Positioned(
                    bottom: -60, left: MediaQuery.of(context).size.width /2 -55,
                    child: myIconInContainer(
                      color1: anCyan ,color2: Colors.white, heightWidth: 120,
                      widget: Icon(Icons.account_circle ,size: 110 ,color: anCyan,) ,
                    )),

              ],
            ),
            SizedBox(height: 70),

            //  TextField phone  =========================================
           myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.phone , vertical: 4,
                    prefixIcon: Icons.phone_android ,HintText: Translations.of(context).text('first_name') , controllers: phoneNumber)
            ,


            //  TextField  Second name =========================================
            myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.text , vertical: 4,
                    prefixIcon: Icons.account_circle ,HintText: Translations.of(context).text('secand_name'),  controllers: secondName)
            ,


            //  Email =========================================
             myTextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.emailAddress , vertical: 4,
                    prefixIcon: Icons.email ,HintText:Translations.of(context).text('email'), controllers: email)
            ,


            // Button next =========================================
            SizedBox(height: 30,),
            myButton(
                onBtnclicked: (){functionButtonNext();} ,
                horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
                textButton: Translations.of(context).text('next') , fontSize: 18
            )

          ],
        ),



    
        ]));
  }
}
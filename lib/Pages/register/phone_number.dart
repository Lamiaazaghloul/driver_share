
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:driver_share/Globals/Tools/print.dart';
import 'package:driver_share/Pages/register/activation_code.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:http/http.dart' as http;

import '../../translations.dart';
class PagePhoneNumber extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PagePhoneNumber> {

//controlers

final _formKey = GlobalKey<FormState>();
 TextEditingController controllerPhone;


  //  ============== ============================اكشن المتابعة
  void functionButtonNext(){
    print("متابعة ");
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PageActivationCode()),
  );
  }
    @override
  void initState() {
    super.initState();
     
  }
 void submit() {
   
    // if (this._formKey.currentState.validate()) {
    //   _formKey.currentState.save();
       printBlue(countrycodes);
      var url = "http://share.riyadasa.com/api/v1/user/login";
      http.post(url, headers: {
        // "Accept": "application/x-www-form-urlencoded",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept-Language":"en"
      }, body: {
        "phone": '01010025337',
        "country_code":countrycodes
      }).then((response) {
        print("Response body: ${response.body}");
        printGreen(json.decode(response.body));
       
      });
   
  // }
  }

CountryCode countryCode;
String countrycodes='EG';
 
void _onCountryChange(countryCode) {
    //Todo : manipulate the selected country code here
    printBlue("New Country selected: " + countryCode.toString());
    printRed(countryCode.code);
    countrycodes=countryCode.code;

  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: anCyan,
        body: new Form(
                            key: this._formKey,
                            child:ListView(
          children: <Widget>[

            // image Logo==========================================
            new Container(
              padding: EdgeInsets.only(top: 150 , left: 50 , right: 50),
              child: Image.asset("asset/Logo1.png",height: 85,),
            ),

            // text phone Number ======================================
            SizedBox(height: 30,),
            Align( alignment: Alignment.center,
                child: new Text(Translations.of(context).text('enter_phone') , style: TextStyle(fontSize: 20 , color: anwhite),)
            ),

            // text TextField =========================================
            SizedBox(height: 30,),

          Row(
               children: <Widget>[
              Container(
                color: Colors.white,
                        width: 100,
                        height: 65,
                        child: new CountryCodePicker(
         onChanged: (val){
           printGreen(val);
           _onCountryChange(val);
         },
         // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
         initialSelection: 'EG',
         favorite: ['+02','EG'],
         // optional. Shows only country name and flag
         showCountryOnly: false,
         // optional. Shows only country name and flag when popup is closed.
        //  showOnlyCountryCodeWhenClosed: false,
         // optional. aligns the flag and the Text left
         alignLeft: false,
       )),
        Expanded(
                        child: myTextFieldMaterial(
                    Radius: 1 ,horizontal: 20 , elevation: 0,textInputType: TextInputType.phone ,
                    prefixIcon: Icons.phone_android ,HintText: Translations.of(context).text('phone_number') ,)
            ,)
               ]),

            // myTextFieldMaterial(
            //         Radius: 1 ,horizontal: 50 , elevation: 0,textInputType: TextInputType.phone ,
            //         prefixIcon: Icons.phone_android ,HintText: "رقم الهاتف")
            // ,


            // text TextField =========================================
            SizedBox(height: 30,),
            myButton(
              onBtnclicked: (){
                // submit();
                functionButtonNext();
                } ,
              horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
              textButton:Translations.of(context).text('Continue') , fontSize: 20
            )

          ],
        ),


     
     ) );
  }
}
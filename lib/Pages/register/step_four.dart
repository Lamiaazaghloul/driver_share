import 'dart:io';
import 'package:driver_share/Pages/Login/login.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';


import '../../translations.dart';


class RegisterFour extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<RegisterFour> {

  File licensePhoto;       // صورة الرخصة
  File iDPhoto;               // صورة الهوية
  File carBack;              // صور السيارة من الخلف
  File carFront;              // صور السيارة من الامام


  //--------- صورة الرخصة من الكاميرا-------------
  Future getLicensePhotoCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context,true);
    setState(() {
      licensePhoto = image;
    });
  }

  //--------- صورة الرخصة من المعرض -------------
  Future getLicensePhotogallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context,true);
    setState(() {
      licensePhoto = image;
    });
  }

  //--------- صورة الهوية من الكاميرا -------------
  Future getIDPhotoCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context,true);
    setState(() {
      iDPhoto = image;
    });
  }

  //--------- صورة الهوية من المعرض -------------
  Future getIDPhotoGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context,true);
    setState(() {
      iDPhoto = image;
    });
  }

  //--------- صور السيارة من الخلف من  الكاميرا -------------
  Future getCarBackCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context,true);
    setState(() {
      carBack = image;
    });
  }

  //--------- صور السيارة من الخلف من  المعرض -------------
  Future getICarBackGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context,true);
    setState(() {
      carBack = image;
    });
  }

  //--------- صور السيارة من الامام  من  الكاميرا -------------
  Future getCarFrontCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context,true);
    setState(() {
      carFront = image;
    });
  }

  //--------- صور السيارة من الامام من  المعرض -------------
  Future getCarFrontGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context,true);
    setState(() {
      carFront = image;
    });
  }



  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";

  void functionButtonNext(){
    // Toast.show("تسجيل", context);
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Login()),
  );  
  }



  @override
  Widget build(BuildContext context) {
    return  new Scaffold(
        backgroundColor: anCyan,


        body: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[

            // Header top=========================================
            new Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                new Container(
                    height: 210, width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: anwhite,
                        image: DecorationImage(
                            colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.1), BlendMode.dstATop),
                            fit: BoxFit.fill,
                            image: NetworkImage(imageURL))
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Align(alignment: Alignment.center, child: Image.asset("asset/Logo2.png",height: 85,),),
                    )
                )
              ],
            ),
            SizedBox(height: 30),

            new Container(
              child: Row(
                children: <Widget>[

                  //===========================================صـــورة  الرخصــة==
                  Expanded(
                      child: GestureDetector(onTap: (){dialogLicensePhoto(context);},
                        child: Container(
                          decoration: BoxDecoration( color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.all(5),
                          height: 130,
                          child: Center(child: licensePhoto == null ? _imageCar(Translations.of(context).text('image_license') ) : Image.file(licensePhoto,fit: BoxFit.cover,)),
                        ),
                      )),

                  //===========================================صـــورة  الهــويــة==
                  Expanded(
                      child: GestureDetector(onTap: (){dialogIDPhoto(context);},
                        child: Container(
                          decoration: BoxDecoration( color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.all(5),
                          height: 130,
                          child: Center(child: iDPhoto == null ? _imageCar(Translations.of(context).text('Image_ID')) : Image.file(iDPhoto,fit: BoxFit.cover,)),
                        ),
                      )),

                ],
              ),
            ),


            new Container(
              child: Row(
                children: <Widget>[

                  //===========================================صورة السيارة من الخلف==
                  Expanded(
                      child: GestureDetector(onTap: (){dialogCarBack(context);},
                        child: Container(
                          decoration: BoxDecoration( color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.all(5),
                          height: 130,
                          child: Center(child: carBack == null ? _imageCar(Translations.of(context).text('Car_image_from_the_front')) : Image.file(carBack,fit: BoxFit.cover,)),
                        ),
                      )),


                  //===========================================صورة السيارة من الامام==
                  Expanded(
                      child: GestureDetector(onTap: (){dialogCarFront(context);},
                        child: Container(
                          decoration: BoxDecoration( color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.all(5),
                          height: 130,
                          child: Center(child: carFront == null ? _imageCar(Translations.of(context).text('Car_image_from_the_end')) : Image.file(carFront,fit: BoxFit.cover,)),
                        ),
                      )),

                ],
              ),
            ),


            // Button next =========================================
            SizedBox(height: 20,),
            myButton(
                onBtnclicked: (){functionButtonNext();} ,
                horizontal: 100 , radiusButton: 15 , colorButton: anwhite , colorText: Colors.grey,
                textButton:Translations.of(context).text('next'), fontSize: 20
            ),
            SizedBox(height: 10),

          ],
        ),
    );
  }

  Widget _imageCar(String name) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Text(name ,style: TextStyle(color: Colors.grey),),
        new Icon(Icons.image ,size: 70, color: Colors.grey[300],)
      ],);
  }

  //======= _Dialog صـــورة  الرخصــة===========================
  void dialogLicensePhoto(BuildContext context) async {
    switch (await showDialog(context: context,
        builder: (BuildContext context)
        {
          return new SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//                  title: const Text('هل تريد حذف هذا الطالب', style: TextStyle(fontFamily: "Cairo"),),
            children: <Widget>[

              new Icon(Icons.image , size: 100, color: anCyan),
              Center(child: new Text(Translations.of(context).text('select_image_license'),style: TextStyle(fontFamily: "Cairo" , fontSize: 17),)),
              SizedBox(height: 20,),

              new Row( mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  //هنا اذا تم الضغط علي نعم سوف يتم استدعاء دالة حذف العنصر
                  new Container(
                    alignment: Alignment.center, height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: Colors.amber[400], borderRadius: BorderRadius.only(topLeft: Radius.circular(10) ,bottomLeft: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getLicensePhotogallery();},
                        child: Icon(FontAwesomeIcons.images , size: 35, color: Colors.white,)
                    ) ,
                  ),

                  
//هنا اذا تم الضغط علي زر لا سوف يتم العود بدون فعل اي شي
                  new Container(alignment: Alignment.center,
                    height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: anCyan, borderRadius: BorderRadius.only(topRight: Radius.circular(10) ,bottomRight: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getLicensePhotoCamera();},
                      child: Icon(FontAwesomeIcons.cameraRetro , size: 35, color: Colors.white,)
                    ),
                  ),
                ],
              ),

            ],
          );
        }
    ))
    {
      default:
    }
  }

  //======= _Dialog صـــورة  الهــويــة  ===========================
  void dialogIDPhoto(BuildContext context) async {
    switch (await showDialog(context: context,
        builder: (BuildContext context)
        {
          return new SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//                  title: const Text('هل تريد حذف هذا الطالب', style: TextStyle(fontFamily: "Cairo"),),
            children: <Widget>[

              new Icon(Icons.image , size: 100, color: anCyan),
              Center(child: new Text(Translations.of(context).text('select_Image_ID'),style: TextStyle(fontFamily: "Cairo" , fontSize: 17),)),
              SizedBox(height: 20,),

              new Row( mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                 

                  //هنا اذا تم الضغط علي زر لا سوف يتم العود بدون فعل اي شي
                  new Container(alignment: Alignment.center,
                    height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: anCyan, borderRadius: BorderRadius.only(topLeft: Radius.circular(10) ,bottomLeft: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getIDPhotoCamera();},
                      child: Icon(FontAwesomeIcons.cameraRetro , size: 35, color: Colors.white,)
                    ),
                  ),
 //هنا اذا تم الضغط علي نعم سوف يتم استدعاء دالة حذف العنصر
                  new Container(
                    alignment: Alignment.center, height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: Colors.amber[400], borderRadius: BorderRadius.only(topRight: Radius.circular(10) ,bottomRight: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getIDPhotoGallery();},
                        child: Icon(FontAwesomeIcons.images , size: 35, color: Colors.white,)
                    ) ,
                  ),
                ],
              ),

            ],
          );
        }
    ))
    {
      default:
    }
  }

  //======= _Dialog  صور السيارة من الخلف===========================
  void dialogCarBack(BuildContext context) async {
    switch (await showDialog(context: context,
        builder: (BuildContext context)
        {
          return new SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//                  title: const Text('هل تريد حذف هذا الطالب', style: TextStyle(fontFamily: "Cairo"),),
            children: <Widget>[

              new Icon(Icons.image , size: 100, color: anCyan),
              Center(child: new Text(Translations.of(context).text('select_Car_image_from_the_end'),style: TextStyle(fontFamily: "Cairo" , fontSize: 17),)),
              SizedBox(height: 20,),

              new Row( mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                
   //هنا اذا تم الضغط علي نعم سوف يتم استدعاء دالة حذف العنصر
                  new Container(
                    alignment: Alignment.center, height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: Colors.amber[400], borderRadius: BorderRadius.only(topLeft: Radius.circular(10) ,bottomLeft: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getICarBackGallery();},
                        child: Icon(FontAwesomeIcons.images , size: 35, color: Colors.white,)
                    ) ,
                  ),
                  //هنا اذا تم الضغط علي زر لا سوف يتم العود بدون فعل اي شي
                  new Container(alignment: Alignment.center,
                    height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: anCyan, borderRadius: BorderRadius.only(topRight: Radius.circular(10) ,bottomRight: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getCarBackCamera();},
                      child: Icon(FontAwesomeIcons.cameraRetro , size: 35, color: Colors.white,)
                    ),
                  ),

                 

                ],
              ),

            ],
          );
        }
    ))
    {
      default:
    }
  }

  //======= _simple صورة السيارة من الامام  ===========================
  void dialogCarFront(BuildContext context) async {
    switch (await showDialog(context: context,
        builder: (BuildContext context)
        {
          return new SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            children: <Widget>[

              new Icon(Icons.image , size: 100, color: anCyan),
              Center(child: new Text(Translations.of(context).text('select_Car_image_from_the_front'),style: TextStyle(fontFamily: "Cairo" , fontSize: 17),)),
              SizedBox(height: 20,),

              new Row( mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                 
   //هنا اذا تم الضغط علي نعم سوف يتم استدعاء دالة حذف العنصر
                  new Container(
                    alignment: Alignment.center, height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: Colors.amber[400], borderRadius: BorderRadius.only(topLeft: Radius.circular(10) ,bottomLeft: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getCarFrontGallery();},
                        child: Icon(FontAwesomeIcons.images , size: 35, color: Colors.white,)
                    ) ,
                  ),

                  //هنا اذا تم الضغط علي زر لا سوف يتم العود بدون فعل اي شي
                  new Container(alignment: Alignment.center,
                    height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: anCyan, borderRadius: BorderRadius.only(topRight: Radius.circular(10) ,bottomRight: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getCarFrontCamera();},
                      child: Icon(FontAwesomeIcons.cameraRetro , size: 35, color: Colors.white,)
                    ),
                  ),

                
                ],
              ),

            ],
          );
        }
    ))
    {
      default:
    }
  }





}
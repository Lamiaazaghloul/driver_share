class ApiListResponse {
  int status;
  List data;
  String error;
  String key;
  int total;
  ApiListResponse(
      {this.status,
      this.data,
      this.key,
      this.error = "No internet or failed to get data",
      this.total});

  factory ApiListResponse.fromJson(
      Map<String, dynamic> json, int skip, String listTotalKey) {
    var result = ApiListResponse(
      status: json['status'] is int
          ? json['status']
          : (json['status'] == true ? 200 : 400),
      data: json['data'] as List,
      error: ((json['error'] as String) ?? (json['message'] as String)) ??
          (json['msg'] as String),
      total: json[listTotalKey] as int,
      key: json['key'] ?? "",
    );
    if (result.total == null) {
      result.total = result.data.length < 20
          ? skip + result.data.length
          : skip + result.data.length + 1;
    }
    return result;
  }

  @override
  String toString() {
    return error ?? "There is no error";
  }
}

typedef OnAuthenticate = Future<void> Function(Map<String, dynamic> data);
typedef OnSaveUserData = Future<void> Function(dynamic data);
typedef OnGetUserData = Future<dynamic> Function();
typedef OnGetAuthentication = Future<Map<String, String>> Function();
typedef OnLogOut = Future<void> Function();
typedef OnCheckAuthentication = Future<bool> Function();

class AuthHelper {
  static AuthHelper _instance;
  final OnAuthenticate authenticateDevice;
  final OnGetAuthentication getAuthHeaders;
  final OnCheckAuthentication checkAuthentication;
  final OnLogOut logOutUser;
  final OnSaveUserData saveUserData;
  final OnGetUserData getUserData;
  static AuthHelper get  instance => _instance ?? AuthHelper.initialize();
  AuthHelper.initialize({
    this.authenticateDevice = defaultAuthenticateDevice,
    this.getAuthHeaders = defaultGetAuthHeaders,
    this.checkAuthentication = defaultCheckAuthentication,
    this.logOutUser = defaultLogOutUser,
    this.saveUserData = defaultSaveUserData,
    this.getUserData = defaultGetUserData,
  }) {
    _instance = this;
  }

  static Future<void> defaultAuthenticateDevice(Map<String, dynamic> data) {
    throw Exception(
        "You dont intialized provider with authenticateDevice parameter");
  }

  static Future<Map<String, String>> defaultGetAuthHeaders() {
    throw Exception(
        "You dont intialized provider with getAuthHeaders parameter");
  }

  static Future<bool> defaultCheckAuthentication() {
    throw Exception(
        "You dont intialized provider with checkAuthentication parameter");
  }

  static Future<void> defaultLogOutUser() {
    throw Exception("You dont intialized provider with logOutUser parameter");
  }

  static Future<void> defaultSaveUserData(data) {
    throw Exception("You dont intialized provider with saveUserData parameter");
  }

  static Future defaultGetUserData() {
    throw Exception("You dont intialized provider with getUserData parameter");
  }
}

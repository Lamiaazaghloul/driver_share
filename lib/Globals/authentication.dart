
import 'package:shared_preferences/shared_preferences.dart';

import 'api/authHelper.dart';


String tokenValue = "";
Future<void> authenticateDevice(Map<String, dynamic> data) async {
  if (data.keys.any((key) => key == "api_token") &&
      data.keys.any((key) => key == "is_active") &&
      (data["is_active"]==1||data["is_active"]=="1")) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("api_token", data["api_token"]);
    tokenValue = data["api_token"];
  }
}

Future<bool> checkAuthentication() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey("api_token")) {
    tokenValue = prefs.getString("api_token");
    return true;
  } else {
    tokenValue = "";
    return false;
  }
}

Future<Map<String, String>> getAuthHeaders() async {
  Map<String, String> headers = Map<String, String>();
  if (tokenValue != null && tokenValue.length > 1) {
    headers["api_token"] = tokenValue;
    return headers;
  }
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey("api_token")) {
    headers["api_token"] = prefs.getString("api_token");
  }
  return headers;
}




void initializeAuthentication() {
  AuthHelper.initialize(
    authenticateDevice: authenticateDevice,
    checkAuthentication: checkAuthentication,
    getAuthHeaders: getAuthHeaders,
  );
}

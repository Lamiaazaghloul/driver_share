import 'package:flutter/material.dart';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../translations.dart';
class SortbBy extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<SortbBy> {

  bool value1 = false;
  bool value2 = false;
  bool value3 = false;
  bool value4 = false;
  bool value5 = false;
  bool value6 = false;

  void _onChangedValue1( bool value){
    setState(() {
      value1 = value;
      if(value1 == true){ print("No "); }
      else{ print("Off"); }
      });
  }


  void _onChangedValue2( bool value){
    setState(() {
      value2 = value;
      if(value2 == true){ print("No "); }
      else{ print("Off"); }
      });
  }

  void _onChangedValue3( bool value){
    setState(() {
      value3 = value;
      if(value3 == true){ print("No "); }
      else{ print("Off"); }
      });
  }

  void _onChangedValue4( bool value){
    setState(() {
      value4 = value;
      if(value4 == true){ print("No "); }
      else{ print("Off"); }
      });
  }

  void _onChangedValue5( bool value){
    setState(() {
      value5 = value;
      if(value5 == true){ print("No "); }
      else{ print("Off"); }
      });
  }

  void _onChangedValue6( bool value){
    setState(() {
      value6 = value;
      if(value6 == true){ print("No "); }
      else{ print("Off"); }
      });
  }



  @override
  Widget build(BuildContext context) {
    return  new Scaffold(


  body: ListView(
    children: <Widget>[

      Container(child: Column(children: <Widget>[

        //=============================== ترتيب حسب  ==
        Padding(
          // padding: const EdgeInsets.symmetric(horizontal: 10),
           padding: const EdgeInsets.only(top: 10),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[

              new Row(
                children: <Widget>[
                  InkWell(onTap: (){ print("asd"); },
                  child: new CircleAvatar(backgroundColor: anCyan,
                  child: Icon(FontAwesomeIcons.sortAmountDown,color: Colors.white,),)),
                  SizedBox(width: 10),
                  new Text(Translations.of(context).text('sort_by'), style: TextStyle(fontSize: 18),),
                ],
              ),

              myButton(textButton:Translations.of(context).text('done')  ,colorButton: anCyan, fontSize: 18 , onBtnclicked: (){},
                  radiusButton: 50 ,heightButton: 40),

            ],),
        ),

        SizedBox(height: 10),


        new Divider(height: 1),

        new ListTile( title: Text(Translations.of(context).text('nearst'),style: TextStyle(fontSize: 16,color: Colors.grey),),
            leading: Icon(Icons.location_on), trailing: new Checkbox(value: value1, onChanged: _onChangedValue1) ),
        new Divider(height: 1,),

        new ListTile( title: Text(Translations.of(context).text('low_price') ,style: TextStyle(fontSize: 16,color: Colors.grey),),
            leading: Icon(FontAwesomeIcons.handHoldingUsd),  trailing: new Checkbox(value: value2, onChanged: _onChangedValue2) ),
        new Divider(height: 1,),

        new ListTile( title: Text(Translations.of(context).text('new_car'),style: TextStyle(fontSize: 16,color: Colors.grey),),
            leading: Icon(Icons.directions_car),  trailing: new Checkbox(value: value3, onChanged: _onChangedValue3) ),
        new Divider(height: 1,),

        new ListTile( title: Text(Translations.of(context).text('old_car'),style: TextStyle(fontSize: 16,color: Colors.grey),),
            leading: Icon(FontAwesomeIcons.taxi),  trailing: new Checkbox(value: value4, onChanged: _onChangedValue4) ),
        new Divider(height: 1,),

        new ListTile(title: Text(Translations.of(context).text('larg_car'),style: TextStyle(fontSize: 16,color: Colors.grey),),
            leading: Icon(FontAwesomeIcons.carSide),  trailing: new Checkbox(value: value5, onChanged: _onChangedValue5) ),
        new Divider(height: 1,),

        new ListTile( title: Text(Translations.of(context).text('small_car'),style: TextStyle(fontSize: 16,color: Colors.grey),),
            leading: Icon(FontAwesomeIcons.truckPickup),  trailing: new Checkbox(value: value6, onChanged: _onChangedValue6) ),

      ],),)

  ],
  ),


      
    );
  }
}
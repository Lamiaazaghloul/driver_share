import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';
import '../translations.dart';

class My_Drawer extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<My_Drawer> {
  @override
  Widget build(BuildContext context) {
    return  new Scaffold(

        //===AppBar================================
        appBar: AppBar(
          elevation: 0,
          title: Text(" Drawer "),
        ),

        //===Drawer================================
        drawer: Drawer(
          child: Directionality(textDirection: TextDirection.rtl,
            child: ListView(
              children: <Widget>[

                new DrawerHeader(
                child: Text(Translations.of(context).text('Select_your_search_filter_options_as_desired'),style: TextStyle(fontSize: 20 , color: angrey),
                textAlign: TextAlign.center),
                ),

                //============================ مكان الانطاق  ===
                Padding(
                  padding: const EdgeInsets.only( left: 10 , right: 10 ),
                  child: new Text(Translations.of(context).text('Starting_place'), style: TextStyle(color: angrey),),
                ),

                myTextFieldMaterial( prefixIcon: Icons.my_location , elevation: 0 ,
                HintText: Translations.of(context).text('Click_here_to_locate_the_destination') , horizontal: 10 ,
                ),

                //============================ مكان الوصول  ===
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only( left: 10 , right: 10 ),
                  child: new Text(Translations.of(context).text('END_place'), style: TextStyle(color: angrey),),
                ),

                myTextFieldMaterial( prefixIcon: Icons.location_on , elevation: 0 ,
                HintText: Translations.of(context).text('Click_here_to_locate_the_tweet') , horizontal: 10 ,
                ),

                //============================ مكان الانطاق  ===
                SizedBox(height: 5),
                Padding(
                  padding: const EdgeInsets.only( left: 10 , right: 10 ),
                  child: new Text(Translations.of(context).text('time_leavel') , style: TextStyle(color: angrey),),
                ),

                myTextFieldMaterial( prefixIcon: Icons.developer_board , elevation: 0 ,
                HintText: "الثلاثاء 5/5/1991" , horizontal: 10 ,
                ),

              ],
            ),
          ),
        ),


        body: Directionality(
          textDirection: TextDirection.rtl,
          child: ListView(
            children: <Widget>[



              //============================ عدد الركاب   ===
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only( left: 10 , right: 10 ),
                child: new Text(Translations.of(context).text('number_user') , style: TextStyle(color: angrey),),
              ),

              new Container(
                color: Colors.grey[200], height: 50,
                child: Row(children: <Widget>[
                  Icon(Icons.account_circle , size: 20,),
                  new VerticalDivider(),

                ],),
              ),


              //============================ النوع    ===
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only( left: 10 , right: 10 ),
                child: new Text(Translations.of(context).text('number_user')  , style: TextStyle(color: angrey),),
              ),

              new Container(
                color: Colors.grey[200], height: 50,
                child: Row(children: <Widget>[
                  Icon(Icons.account_circle , size: 20,),
                  new VerticalDivider(),

                ],),
              ),


              //============================ متاح التدخين   ===
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only( left: 10 , right: 10 ),
                child: new Text(Translations.of(context).text('number_user')  , style: TextStyle(color: angrey),),
              ),

              new Container(
                color: Colors.grey[200], height: 50,
                child: Row(children: <Widget>[
                  Icon(Icons.account_circle , size: 20,),
                  new VerticalDivider(),

                ],),
              ),



              //============================ مكانك الان    ===
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only( left: 10 , right: 10 ),
                child: new Text(Translations.of(context).text('number_user')  , style: TextStyle(color: angrey),),
              ),

              new Container(
                color: Colors.grey[200], height: 50,
                child: Row(children: <Widget>[
                  Icon(Icons.account_circle , size: 20,),
                  new VerticalDivider(),

                ],),
              ),




              SizedBox(),
              myButton( onBtnclicked: (){} , horizontal: 40  ,radiusButton: 5 , textButton: "تم " ,fontSize: 20),








            ],
          ),
        ),




    
    );
  }
}
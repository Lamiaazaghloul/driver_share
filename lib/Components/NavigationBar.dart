
import 'package:driver_share/Pages/Tabs/notifications.dart';
import 'package:driver_share/Pages/Tabs/offers/tabs_offer.dart';
import 'package:driver_share/Pages/Tabs/orders/order.dart';

import 'package:driver_share/Pages/prices/price_tabs.dart';
import 'package:flutter/material.dart';

import '../translations.dart';

class NavigationBar extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<NavigationBar> {

  int _pageIndex = 0;

  // هنا الصفحات التي سوف يتم التنقل بينهم عن طريق NavigationBar
  final OrdersPage home = new OrdersPage();
  final TabsOffer offers = new TabsOffer();
  final PageTabBarView sharePrice = new PageTabBarView();
  final MyNotifcation messages = new MyNotifcation();
  // final ProfilePage _Profile = new ProfilePage();

  Widget _showpage = new OrdersPage();
  Widget _pageChooser(int page){
    switch(page){
      case 0 : return home ;
      break;

      case 1 : return offers ;
      break;

      case 2 : return  sharePrice;
      break;

      case 3 : return  messages;
      break;

      // case 4 : return  _Profile;
      // break;
    }
  }



  @override
  Widget build(BuildContext context) {
    return new Scaffold(
  
        //====bottomNavigationBar===============================================
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: new Image.asset("asset/order.png",width:30,), 
            title: Text(Translations.of(context).text('orders'))),
            BottomNavigationBarItem(icon: new Image.asset("asset/discount.png",width:25), 
            title: Text(Translations.of(context).text('offers'))),
            BottomNavigationBarItem(icon: new Image.asset("asset/share.png",width:25),
             title: Text(Translations.of(context).text('share'))),
            BottomNavigationBarItem(icon: new Image.asset("asset/notification.png",width:25),
             title: Text(Translations.of(context).text('notificatios'))),
            // BottomNavigationBarItem(icon: new Image.asset("asset/avatar.png",width:25),
            //  title: Text(Translations.of(context).text('profile'))),
          ],
           type: BottomNavigationBarType.fixed,      // لاظهار باقي العناصر التي تم اختفائها
          currentIndex: _pageIndex,
          elevation: 0,
          unselectedItemColor: Colors.blueGrey,
          backgroundColor: Colors.white,
          onTap: (int _pageIndex){
            setState(() {
              this._pageIndex=_pageIndex;
              _showpage = _pageChooser(_pageIndex);
            });
          },
        ),




        //====body=========================================================
        body: Container(
          child: Center(
            child: _showpage,
          ),
        ),


    );
  }
}
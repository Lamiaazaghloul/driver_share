import 'dart:async';
import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/print.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

// import 'package:location/location.dart' as LocationManager;
// import 'package:location/location.dart';


class GetLcationFromMap extends StatefulWidget {
  @override
  _GetLcationFromMapState createState() => _GetLcationFromMapState();
}

class _GetLcationFromMapState extends State<GetLcationFromMap> {
  
var lat,long;
// test(){
//   var location = new Location();
// location.onLocationChanged().listen((LocationData currentLocation) {
//   printGreen(currentLocation.latitude);
//   printGreen(currentLocation.longitude);
//   lat=currentLocation.latitude;
//   long=currentLocation.longitude;
//   final center = LatLng(currentLocation.latitude, currentLocation.longitude);
//    printBlue(center);
   
// });
// }
    //    Future<LatLng> getUserLocation() async {
    // var currentLocation = <String, double>{};
    // // final locations = LocationManager.Location();
    // try {
    //   currentLocation = (await locations.getLocation()) as Map<String, double>;
    //   final lat1 = currentLocation["latitude"];
    //   final long1 = currentLocation["longitude"];
    //   final center = LatLng(lat1, long1);
    //   printGreen(center);
    //   printBlue(lat1);
    //   return center;
    // } on Exception {
    //           printRed("من فضلك تاكيد من فتح الموقع الجغرافي");
     
    //   }
    //   currentLocation = null;
    //   return null;
    // }
  



  Completer<GoogleMapController> _controller = Completer();

  
  static const LatLng _center = const LatLng(27.1889161, 31.1664061);

  final Set<Marker> _markers = {};

  LatLng _lastMapPosition = _center;

  MapType _currentMapType = MapType.normal;

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(_lastMapPosition.toString()),
        position: _lastMapPosition,
        infoWindow: InfoWindow(
          title: _lastMapPosition.toString(),
          snippet: '5 Star Rating',
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));
    });
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  //  getUserLocation();
  // test();
    _onAddMarkerButtonPressed();
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
          children: <Widget>[
            GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 11.0,
              ),
              mapType: _currentMapType,
              markers: _markers,
              onCameraMove: _onCameraMove,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Align(
                alignment: Alignment.topRight,
                child: Column(
                  children: <Widget> [
                    FloatingActionButton(
                      onPressed: _onMapTypeButtonPressed,
                      materialTapTargetSize: MaterialTapTargetSize.padded,
                      backgroundColor: anCyan,
                      child: const Icon(Icons.map, size: 36.0),
                    ),
                    SizedBox(height: 16.0),
                    FloatingActionButton(
                      onPressed: _onAddMarkerButtonPressed,
                      materialTapTargetSize: MaterialTapTargetSize.padded,
                      backgroundColor:anCyan,
                      child: const Icon(Icons.add_location, size: 36.0),
                    ),
                  ],
                ),
              ),
            ),
          
          ],
        
      
    
    ));
  }
}
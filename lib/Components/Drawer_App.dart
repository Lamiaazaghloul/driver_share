import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Pages/My%20Share/my_share.dart';
import 'package:driver_share/Pages/Tabs/messages.dart';
import 'package:driver_share/Pages/Tabs/profile.dart';
import 'package:driver_share/Pages/Trips/tabs_trips.dart';
import 'package:flutter/material.dart';

import '../translations.dart';
import 'NavigationBar.dart';


class DrawerApp extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<DrawerApp> {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Center(child: Image.asset("asset/Logo1.png")),
              decoration: BoxDecoration(
                color: anCyan
              ),
            ),

            new ListTile(
              title: Text(Translations.of(context).text("home"),style: TextStyle(color: Color(0xff4b4a4a)),),
              leading: CircleAvatar(backgroundColor: Colors.grey[100] , child: Image.asset("asset/homes.png",width: 30,)),
              onTap: (){
                Navigator.push(context, MaterialPageRoute
                (builder: (context) => NavigationBar()),);
                },
              trailing: Icon(Icons.arrow_forward_ios,size: 20,),
            ),
            new Divider(),
             new ListTile(
              title: Text(Translations.of(context).text("trip"),style: TextStyle(color: Color(0xff4b4a4a)),),
              leading: CircleAvatar(backgroundColor: Colors.grey[100] , child: Image.asset("asset/car.png",width: 30,)),
              onTap: (){
                Navigator.push(context, MaterialPageRoute
                (builder: (context) => TabsTrips()),);
                },
              trailing: Icon(Icons.arrow_forward_ios,size: 20,),
            ),
            new Divider(),

              new ListTile(
              title: Text(Translations.of(context).text("mySahre"),style: TextStyle(color: Color(0xff4b4a4a)),),
              leading: CircleAvatar(backgroundColor: Colors.grey[100] , child: Image.asset("asset/group.png",width: 30,)),
              onTap: (){
                Navigator.push(context, MaterialPageRoute
                (builder: (context) => Myposts()),);
                },
              trailing: Icon(Icons.arrow_forward_ios,size: 20,),
            ),
            new Divider(),


             new ListTile(
              title: Text(Translations.of(context).text("messages"),style: TextStyle(color: Color(0xff4b4a4a)),),
              leading: CircleAvatar(backgroundColor: Colors.grey[100] , child: Image.asset("asset/message.png",width: 30,)),
              onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Messages()),);},
              trailing: Icon(Icons.arrow_forward_ios,size: 20,),
            ),
            new Divider(),

            new ListTile(
              title: Text(Translations.of(context).text("myprofile"),style: TextStyle(color: Color(0xff4b4a4a)),),
              leading: CircleAvatar(backgroundColor: Colors.grey[100] , child: Image.asset("asset/user.png",width: 30,)),
              onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()),);},
              trailing: Icon(Icons.arrow_forward_ios,size: 20,),
            ),
            new Divider(),

           


            new ListTile(
              title: Text(Translations.of(context).text("setting"),style: TextStyle(color: Color(0xff4b4a4a)),),
              leading: CircleAvatar(backgroundColor: Colors.grey[100] , child: Image.asset("asset/settings.png",width: 30,)),
              onTap: (){
                // Navigator.push(context, MaterialPageRoute(builder: (context) => ContactUs()),);
                },
              trailing: Icon(Icons.arrow_forward_ios,size: 20,),
            ),
            new Divider(),


          ],
        ),
    
    );
  }
}
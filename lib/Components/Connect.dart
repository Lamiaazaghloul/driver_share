import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';

Widget circleImageOreder(String image ,{Color colorActiveImage = Colors.green}) {
  return Container(
    child: Center(child:
    Stack(children: <Widget>[
      new Container(
        width: 60,
        height: 60,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.white , width: 0),
            image: DecorationImage(
                fit: BoxFit.cover,
                image: new AssetImage('asset/photo.png'))
        ),
      ),
      // Positioned(
      //     bottom: 0 , left: 10,
      //     child: new CircleAvatar(maxRadius: 5, backgroundColor: Colors.white,
      //       child:CircleAvatar(maxRadius: 5,backgroundColor: colorActiveImage,),
      //     ))
    ],
    ),
    ),
  );
}


Widget circleImage(String image ,{Color colorActiveImage = Colors.green}) {
  return Container(
    child: Center(child:
    Stack(children: <Widget>[
      new Container(
        width: 60,
        height: 60,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.white , width: 0),
            image: DecorationImage(
                fit: BoxFit.cover,
                image:new AssetImage('asset/photo.png'))
        ),
      ),
      Positioned(
          bottom: 0 , left: 10,
          child: new CircleAvatar(maxRadius: 5, backgroundColor: Colors.white,
            child:CircleAvatar(maxRadius: 5,backgroundColor: colorActiveImage,),
          ))
    ],
    ),
    ),
  );
}

//message list 

Widget myMessageList({
  String price = '100',
  String carType = "كامري",
  String timeStart = "13:10",
  String timeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String number = "3 / 4" ,
  String dateStart = "16يناير" ,
  String dateEnd = "16يناير" ,
  String imageUrl = "",
  String chating="مراسلة",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 140,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

       
 //==================================صورة العميل  =
        new Expanded(flex: 2, child: circleImage(imageUrl , colorActiveImage: colorActive)),
        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 4,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  new Text("كيا سيراتو" , style: TextStyle(fontSize: 15),),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                       Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.account_circle,size: 14,color: Colors.grey),
                      ),
                      Text("محمد جمال" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                     
                    ],
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                       Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.message,size: 14,color: Colors.grey),
                      ),
                      Text("السلام عليكم ورحمه الله" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                     
                    ],
                  ),



                ],
              ),
            )
        ),

 //==================================السعر وحجز الان   =
        new Expanded(flex: 2,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  myButton(fontSize: 12,colorButton: anCyan,textButton: chating,horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),
       
      ],
    ),
  );
}


Widget requestAirportList({
  String price = '100',
  String carType = "حجم السياره وسط",
  String timeStart = "13:10",
  String timeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String number = "3 / 4" ,
  String dateStart = "16يناير" ,
  String dateEnd = "16يناير" ,
  String imageUrl = "",
  String offer="قدم عرض",
  String sizeCar="حجم السيارة",
  String air="مكيف",
  String noSmok="ممنوع التدخين",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 120,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[


//==================================صورة العميل  =
        new Expanded(flex: 2, child: circleImage(imageUrl , colorActiveImage: colorActive)),
        //================================================== الخط ونقاط التوصيل  ===
        new Expanded(flex: 1,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                children: <Widget>[


                  //==============================================الخط الي يظهر تحت المنطقة ==
                  Expanded(
                      child: Stack(children: <Widget>[
                        //----- هذا الخط
                        Padding(
                          padding: const EdgeInsets.only(left:10 , right: 10 ,top: 10 , bottom: 10),
                          child: new Container(width: 1 , color: colorActive,),
                        ),

                        //----- هذا دائرة اليسار
                        Positioned(top: 8, right: 1,left: 1,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                              child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                                child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),

                        //----- هذا دائرة اليمين
                        Positioned(
                            bottom: 8,right: 0,left: 1,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                            child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                            child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),
                      ],)),


                ],
              ),
            )
        ),
 //================================================== مكان الاقلاع والوقت والتاريخ ===
        new Expanded(flex: 1,
            child: Container(
               decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container( height: 40,
                    child: Center(child: Stack(overflow: Overflow.visible,
                          children: <Widget>[
                          Positioned(left: 4,top: -8,child: Text("${nameFrom}", style: TextStyle(fontSize: 8,color: Colors.grey),)),
                          Positioned(left: 4,bottom: -8,child: Text("${dateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                          Text("${timeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                        ]))),

                    new Container( height: 40,
                    child: Center(child: Stack(overflow: Overflow.visible,
                          children: <Widget>[
                          Positioned(left: 4,top: -8,child: Text("${nameFrom}", style: TextStyle(fontSize: 8,color: Colors.grey),)),
                          Positioned(left: 4,bottom: -8,child: Text("${dateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                          Text("${timeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                        ]))),
                ],
              ),
            )
        ),
//==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                     Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    ),
                    Text(air ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                   
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                     Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    ),
                    Text(noSmok,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                   
                  ],
                ),

                new Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                     Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    ),
                    Text("${number}" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                   
                  ],
                ),


              ],),
            )
        ),

       

        //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(padding: EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                          border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.directions_car,size: 15,color: Colors.grey),
                          Text("${carType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                        ],
                      )),

                
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  myButton(fontSize: 12,colorButton: anCyan,textButton: offer,horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        
 
       
      ],
    ),
  );
}
  

////////////////////////////////////driver


Widget myOrdersList({
  String price = '100',
  String carType = "كامري",
  String timeStart = "13:10",
  String timeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String number = "3 / 4" ,
  String dateStart = "16يناير" ,
  String dateEnd = "16يناير" ,
  String rail="ريال",
  String imageUrl = "",
  String book ="قدم عرض",
  String takeUserFromHome="يسمح باخذ الركاب من المنزل",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
  GestureTapCallback details
}) {
  return new GestureDetector(
    onTap: details,
    child:new Container(
    height: 120,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

      
  //==================================صورة العميل  =
        new Expanded(flex: 2, child: circleImageOreder(imageUrl , colorActiveImage: colorActive)),

        //==================================البيانات الداخلية  ويوجد بها قسمين   =
        new Expanded(flex: 7,child:
        Container(child: Row(
          children: <Widget>[
            //================================== القسم الاول من والي  / يسمع بأخذ الراكب من المنزل  =
            Expanded(
              flex: 6,child: Container(
              decoration: BoxDecoration(
              border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                children: <Widget>[

                //================================================ من الرياض الي جدة ===
                Padding(
                  padding: const EdgeInsets.only(left: 5 ,right: 5 , top: 3 ,bottom: 3),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("${nameFrom}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                      Text("${nameTo}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                    ],
                  ),
                ),



                //==============================================الخط الي يظهر تحت المنطقة ==
                Expanded(
                  child: Stack(children: <Widget>[
                  //----- هذا الخد
                  Padding(
                    padding: const EdgeInsets.only(left: 5 , right: 5 ,top: 6),
                    child: new Container(height: 1 , color: colorActive,),
                  ),

                  //----- هذا دائرة اليسار
                  Positioned(left: 5,
                      child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                      child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                      child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                      )),

                  //----- هذا دائرة اليمين
                  Positioned(
                    right: 5,
                    child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                    child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                    child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                    )),

                  //----- هذا دائرة الوسط
                  Positioned(
                    right: 5, left: 5,
                    child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                    child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                    child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                    )),

                ],)),

                //=============================== وقت القلاع والوصول وتاريخ الوصول =
                Padding(
                  padding: const EdgeInsets.only(left: 4 , right: 4 ,bottom: 17),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                    new Column(children: <Widget>[
                        Stack(overflow: Overflow.visible,
                            children: <Widget>[
                              Positioned(left: 4,bottom: -8,child: Text("${dateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                              Text("${timeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                            ])
                      ],),

                    new Column(children: <Widget>[
                        Stack(overflow: Overflow.visible,
                            children: <Widget>[
                              Positioned(left: 4,bottom: -8,child: Text("${dateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                              Text("${timeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                            ])
                      ],),

                    new Column(children: <Widget>[
                        Stack(overflow: Overflow.visible,
                            children: <Widget>[
                              Positioned(left: 4,bottom: -8,child: Text("${dateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                              Text("${timeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                            ])
                      ],),

                    ],),
                ),

                //============================================== يسمح بأخد الراكب من المنزل و عدد الركاب ==
                Padding(
                  padding: const EdgeInsets.only(left: 5 , right: 0,bottom: 13 ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                        Icon(Icons.note,size: 12,color: Colors.grey,),
                      Text(takeUserFromHome, style: TextStyle(fontSize: 9,color: Colors.grey),),
SizedBox(width: 10,),
                      Icon(Icons.directions_car,size: 12,color: Colors.grey,),
                      Text(number, style: TextStyle(fontSize: 9,color: Colors.grey),),

                      
                    
                    ],
                  ),
                ),



              ],),
            )
            ),

          ],
        ),)),


  //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  myButton(fontSize: 12,colorButton: anCyan,textButton: book,horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),
      
      ],
    ),
  ));
}


//////////////////////////////////////////////////////

Widget myOffersList({
  String price = '100',
  String carType = "كامري",
  String timeStart = "13:10",
  String timeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String number = "3 / 4" ,
  String dateStart = "16يناير" ,
  String dateEnd = "16يناير" ,
  String imageUrl = "",
  String book="احجز",
  String offerName="تأجير سياره",
  Color colorActive = Colors.green,
  Color colorBG = Colors.white,
  Color colorType=anCyan,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 120,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: colorBG,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[
//==================================صورة العميل  =
        new Expanded(flex: 3, child: circleImage(imageUrl , colorActiveImage: colorActive)),
        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 4,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  new Text(offerName, style: TextStyle(fontSize: 15),),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.account_circle,size: 11,color: Colors.grey),
                      ),
                      Text("محمد جمال" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      
                    ],
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.assignment_ind,size: 11,color: Colors.grey),
                      ),
                      Text("ذكر" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      
                    ],
                  ),



                ],
              ),
            )
        ),

      
        // //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        // new Expanded(flex: 5,
        //     child: Container(
        //       decoration: BoxDecoration(
        //           border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
        //       ),
        //       child: Column(
        //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: <Widget>[

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.start,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //              Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //             ),
        //             Text("مكيف" ,style: TextStyle(fontSize:10,color: Colors.grey),),
                   
        //           ],
        //         ),
        //         // new Divider(),

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.start,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //             ),
        //             Text("ممنوع التدخين " ,style: TextStyle(fontSize:10,color: Colors.grey),),
                    
        //           ],
        //         ),
        //         // new Divider(),

        //         // Row(
        //         //   mainAxisAlignment: MainAxisAlignment.end,
        //         //   crossAxisAlignment: CrossAxisAlignment.center,
        //         //   children: <Widget>[
        //         //     Padding(
        //         //       padding: const EdgeInsets.only(right: 5,left: 5),
        //         //       child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //         //     ),
        //         //     Text("اخد الراكب من المنزل" ,style: TextStyle(fontSize:10,color: Colors.grey),),
                    
        //         //   ],
        //         // ),


        //       ],),
        //     )
        // ),

  //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                 

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  myButton(fontSize: 10,colorButton: colorType,textButton: book,horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

  

      
      ],
    ),
  );
}


Widget myOrderDetails({
  String price = '100',
  String carType = "كامري",
  String timeStart = "13:10",
  String timeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String number = "3 / 4" ,
  String book ="احجز",
  String dateStart = "16يناير" ,
  String dateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  Color colorBG = Colors.white,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 140,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: colorBG,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

       
 //==================================صورة العميل  =
        new Expanded(flex: 3, child: circleImage(imageUrl , colorActiveImage: colorActive)),

        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 4,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  new Text("محمد جمال" , style: TextStyle(fontSize: 15),),

  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.assignment_ind,size: 11,color: Colors.grey),
                      ),
                      Text("ذكر" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                     

                    ],
                  ),
                 


                



                ],
              ),
            )
        ),

//==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        new Expanded(flex: 6,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    ),
                    Text("مكيف" ,style: TextStyle(fontSize:10,color: Colors.grey),),

                  ],
                ),
                // new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    ),
                    Text("ممنوع التدخين " ,style: TextStyle(fontSize:10,color: Colors.grey),),

                  ],
                ),
                // new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.gps_fixed,size: 15,color: Colors.grey),
                    ),
                    Text("51 متر" ,style: TextStyle(fontSize:12,color: Colors.grey),),

                  ],
                ),


              ],),
            )
        ),

    

             ],
    ),
  );
}




Widget mySubOreserDetails({
  String price = '100',
  String carType = "كامري",
  String timeStart = "13:10",
  String timeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String number = "3 / 4" ,
  String dateStart = "16يناير" ,
  String dateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 120,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(8),
    ),
    child: Row(
      children: <Widget>[

        //==================================البيانات الداخلية  ويوجد بها قسمين   =
        new Expanded(flex: 6,child:
        Container(child: Row(
          children: <Widget>[

            //================================== القسم الاول من والي  / يسمع بأخذ الراكب من المنزل  =
            Expanded(
                flex: 6,child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                children: <Widget>[

                  //================================================ من الرياض الي جدة ===
                  Padding(
                    padding: const EdgeInsets.only(left: 5 ,right: 5 , top: 5 ,bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("${nameFrom}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                        Text("${nameTo}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                      ],
                    ),
                  ),



                  //==============================================الخط الي يظهر تحت المنطقة ==
                  Expanded(
                      child: Stack(children: <Widget>[
                        //----- هذا الخد
                        Padding(
                          padding: const EdgeInsets.only(left: 5 , right: 5 ,top: 6),
                          child: new Container(height: 1 , color: colorActive,),
                        ),

                        //----- هذا دائرة اليسار
                        Positioned(left: 5,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                              child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                                child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),

                        //----- هذا دائرة اليمين
                        Positioned(
                            right: 5,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                              child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                                child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),

                      ],)),


                  Padding(
                    padding: const EdgeInsets.only(left: 4 , right: 4 ,bottom: 45),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[

                        new Column(children: <Widget>[
                          Stack(overflow: Overflow.visible,
                              children: <Widget>[
                                Positioned(left: 4,bottom: -8,child: Text("${dateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                                Text("${timeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                              ])
                        ],),

                        new Column(children: <Widget>[
                          Stack(overflow: Overflow.visible,
                              children: <Widget>[
                                Positioned(left: 4,bottom: -8,child: Text("${dateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                                Text("${timeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                              ])
                        ],),


                      ],),
                  ),


                  //============================================== يسمح بأخد الراكب من المنزل ==
                  // Padding(
                  //   padding: const EdgeInsets.only(left: 0 , right: 0,bottom: 7 ),
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     children: <Widget>[
                  //       SizedBox(width: 3),



                  //       SizedBox(width: 20),
                  //       Text("ممنوع التدخين", style: TextStyle(fontSize: 9,color: Colors.grey),),
                  //       Icon(Icons.smoke_free,size: 12,color: Colors.grey,),

                  //       SizedBox(width: 20),
                  //       Text("3 / 4", style: TextStyle(fontSize: 9,color: Colors.grey),),
                  //       Icon(Icons.directions_car,size: 12,color: Colors.grey,),

                  //       SizedBox(width: 20),
                  //       Text("كيا سيراتو", style: TextStyle(fontSize: 9,color: Colors.grey),),
                  //       Icon(Icons.directions_car,size: 12,color: Colors.grey,),

                  //       SizedBox(width: 20),
                  //       Text("يسمح بأخد الراكب من المنزل", style: TextStyle(fontSize: 9,color: Colors.grey),),
                  //       Icon(Icons.home,size: 12,color: Colors.grey,),


                  //     ],
                  //   ),
                  // ),



                ],),
            )
            ),

          ],
        ),)),

      ],
    ),
  );
}

Widget myNotificatioList({
  String price = '100',
  String carType = "كامري",
  String timeStart = "13:10",
  String timeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String number = "3 / 4" ,
  String dateStart = "16يناير" ,
  String dateEnd = "16يناير" ,
  String imageUrl = "",
  String chating="مراسلة",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 90,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

       
 //==================================صورة العميل  =
        new Expanded(flex: 2, child: circleImage(imageUrl , colorActiveImage: colorActive)),
        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 4,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  new Text("كيا سيراتو" , style: TextStyle(fontSize: 15),),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                       Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.account_circle,size: 14,color: Colors.grey),
                      ),
                      Text("محمد جمال" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                     
                    ],
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                       Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.message,size: 14,color: Colors.grey),
                      ),
                      Text("السلام عليكم ورحمه الله" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                     
                    ],
                  ),



                ],
              ),
            )
        ),


       
      ],
    ),
  );
}

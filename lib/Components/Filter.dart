import 'package:driver_share/Globals/Tools/StyleApp.dart';
import 'package:driver_share/Globals/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../translations.dart';




class Filter extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<Filter> {


  //====== FunctionButtton تم =================
  void functionButtton(){
    Toast.show(" تم الفلتر  ", context);
  }


  //====== Function محطات   =================
  bool value1 = true;
  void _onChanged(bool value){
    setState(() {
      value1 = value ;
      if(value == true){
        Toast.show(" No  ", context);
      }
      else{  Toast.show("OFF  ", context); }
    });
  }


  //====== Function حجم السيارة   =================
  int radioGroupCar = 1;
  void _radioCar(int radioValue){
    setState(() {
      radioGroupCar = radioValue;
      switch(radioGroupCar)
      {
        case 1:  Toast.show(" كبيرة  ", context);
        break;

        case 2:    Toast.show(" متوسطة   ", context);
         break;

        case 3:   Toast.show(" ضغيرة  ", context);
         break;
      }
    });
  }


  //======  السعر من الي   =================
  TextEditingController _priceFrom = new TextEditingController();
  TextEditingController _priceTo = new TextEditingController();



  //====== Function حجم السيارة   =================
  int radioGroupDriver = 1;
  void _radiodriver(int radioValue){
    setState(() {
      radioGroupDriver = radioValue;
      switch(radioGroupDriver)
      {
        case 1:  Toast.show(" ذكر  ", context);
         break;

        case 2:    Toast.show("انثي ", context);
         break;

      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return  new Scaffold(

        //----AppBar-------------------------------------
        body: ListView(
          children: <Widget>[
           Container(
                padding: EdgeInsets.symmetric(horizontal: 10 ),
                child: Column(children: <Widget>[

                //=============================== ترتيب حسب ==
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    new Row(
                      children: <Widget>[

                        Padding(
                          padding: const EdgeInsets.only( left: 5),
                          child: InkWell(onTap: (){ print("asd"); },
                              child: new CircleAvatar(backgroundColor: anCyan,
                                child: Icon(FontAwesomeIcons.sortAmountDown,color: Colors.white,),)),
                        ),
                        new Text(Translations.of(context).text('fillter'), style: TextStyle(fontSize: 18 , color: angrey),),
                      ],
                    ),

                    myButton(textButton:Translations.of(context).text('done')  , fontSize: 16 , onBtnclicked: (){functionButtton();},
                        radiusButton: 50 ,heightButton: 40 ,colorButton: anCyan),

                  ],),
                new Divider(),

                //================= محطات   ==
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Row(children: <Widget>[
                      new Icon(Icons.location_on , color: Colors.grey[500],),
                      new Text(Translations.of(context).text('Stations'), style: TextStyle(fontSize: 17 , color: angrey),),
                    ],),
                    new Checkbox(value: value1, onChanged: _onChanged )
                  ],
                ),
                new Divider(),


                //-----------احجام السارة--
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.directions_car , color: Colors.blue),
                    Text(Translations.of(context).text('size_car'),style: TextStyle(color: Colors.blue , fontSize: 11 ,),),
                  ],),


                //=================احجام السيارة   ==
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    //----------- كبيرة--
                    new Row(
                      children: <Widget>[
                        new Radio(value: 1, groupValue: radioGroupCar, onChanged: _radioCar),
                        Text(Translations.of(context).text('larg') ,style: TextStyle(color: angrey, fontSize: 13),),
                      ],),

                    //----------- متوسطة--
                    new Row(
                      children: <Widget>[
                        new Radio(value: 2, groupValue: radioGroupCar, onChanged: _radioCar),
                        Text(Translations.of(context).text('media') ,style: TextStyle(color: angrey, fontSize: 13),),
                      ],),

                    //----------- صغيرة--
                    new Row(
                      children: <Widget>[
                        new Radio(value: 3, groupValue: radioGroupCar, onChanged: _radioCar),
                        Text(Translations.of(context).text('small'),style: TextStyle(color: angrey  , fontSize: 13),),
                      ],),


                  ],),
                new Divider(),

                //================= السعر   ==
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[

                    new Expanded(
                        child: Container(
                          child: Row(
                            children: <Widget>[

                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Icon(FontAwesomeIcons.handHoldingUsd, size: 20, color: angrey),
                              ),
                              Text(Translations.of(context).text('price'), style: TextStyle(color: angrey),)

                            ],),)
                    ),

                    new Expanded(
                        child: Container(
                          child: new Column(children: <Widget>[
                            myTextField(controllers: _priceFrom,Radius: 5 , hintText:Translations.of(context).text('from') , textAlign: TextAlign.center , TextInput: TextInputType.number ,)
                          ],),)
                    ),
                    SizedBox(width: 10),

                    new Expanded(
                        child: Container(
                          child: new Column(children: <Widget>[
                            myTextField(controllers: _priceTo,maxLines: 1,Radius: 5 , hintText:Translations.of(context).text('to') , textAlign: TextAlign.center , TextInput: TextInputType.number ,)
                          ],),)
                    ),

                    new Expanded(
                        child: Container(
                          child: new Text(Translations.of(context).text('real') ,style: TextStyle(fontSize: 15 ,color: angrey),textAlign: TextAlign.center,),)
                    ),

                  ],),

                //================= السعر من الي   ==
                new Divider(),

                //================= نوع السائق  ==
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

                    //-----------احجام السارة--
                    new Row(
                      children: <Widget>[
                        Icon(FontAwesomeIcons.userAlt ,color: angrey, size: 18),
                        SizedBox(width: 10),
                        Text(Translations.of(context).text('gender') ,style: TextStyle(color: angrey),),
                      ],),

                  

                    //----------- متوسطة--
                    new Row(
                      children: <Widget>[
                        new Radio(value: 2, groupValue: radioGroupDriver, onChanged: _radiodriver),
                        Text(Translations.of(context).text('male') ,style: TextStyle(color: angrey),),
                      ],),
  //----------- كبيرة--
                    new Row(
                      children: <Widget>[
                        new Radio(value: 1, groupValue: radioGroupDriver, onChanged: _radiodriver ),
                        Text(Translations.of(context).text('female'),style: TextStyle(color: angrey),),
                      ],),

                  ],),


              ],),),
            
        ],),
     
    );
  }
}
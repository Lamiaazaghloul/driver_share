import 'package:driver_share/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:driver_share/Redux/reducers/appReducers.dart';
import 'Globals/api/apiHelper.dart';
import 'Pages/Tabs/listfield.dart';
import 'Pages/Tabs/profileseeting.dart';
import 'Pages/register/phone_number.dart';
import 'Redux/states/appstate.dart';
import 'allTranslations.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

void main() 
{
  ApiHelper.intialize(
    baseUrl: "http://share.riyadasa.com/api/v1",
    requestHeadersAsParms: true,
    noInternetMessage:"لا نستطيع الاتصال بالسيرفر برجاء التاكد  من توافر الإنترنت  لديك",
    connectionTimeOutMessage: "هذه العملية تستغرق وقت طويل جدا",
    sendingTimeOutMessage: "هذه العملية تستغرق وقت طويل جدا",
    recivingTimeOutMessage:
        "تم إرسال الطلب ولكن استغرقت العملية وقت اطول من اللازم",
  );
  // initializeAuthentication();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // SpecificLocalizationDelegate _localeOverrideDelegate;
  @override
  Widget build(BuildContext context) {

    
    final Store<AppState> store = Store<AppState>(
      appReducer,
      initialState: AppState.initial(),
    );

    return StoreProvider<AppState>(
      store: store,
      child:MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        // _localeOverrideDelegate,
        const TranslationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
 ],
//  supportedLocales: [
//     const Locale('ar'), // arabic
//     const Locale('en'), // English
//     const Locale('tr'), // Turkish
    
 //   ],
  supportedLocales: allTranslations.supportedLocales(),
   locale:  const Locale('ar'),
  //  langcode=='ar'? const Locale('ar'):const Locale('en'),
      title: 'share',
      theme: ThemeData(
        fontFamily: 'cairo',
        accentColor: Colors.cyan[600],
        brightness: Brightness.light,
      textTheme: TextTheme(
      headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
      title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
      body1: TextStyle(fontSize: 14.0, fontFamily: 'cairo'),
    ),
      ),
      home:PlayerList(),
    )
    );
    
  }
}



class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 
  @override
  Widget build(BuildContext context) {
  return new SplashScreen(
      seconds: 3,
      navigateAfterSeconds: MyPets(),
      imageBackground: AssetImage('asset/splash.png'),
      styleTextUnderTheLoader: new TextStyle(),
      onClick: ()=>print("share"),
      loaderColor: Colors.white,
    );
  }
}


